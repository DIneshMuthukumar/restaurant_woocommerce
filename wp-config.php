<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

const JETPACK_DEV_DEBUG = TRUE;



define( 'DB_NAME', 'restuarant_ecommerce' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'v9!m7&(8<bW?#XA4Jy;}=BhU!yGUO5FupGU4I|rww8*{Wcc$~u^]{#yhiJ9;1EEQ' );
define( 'SECURE_AUTH_KEY',  'Q.}JWb1<{2?EWWDkm2LLGJJ2{Y`iJED1#O&m:kLN[g6y$GcvkbQ9Y %;9|l&v8:M' );
define( 'LOGGED_IN_KEY',    ';BlyMv?bYU;jJ0d>yuIV#9v)@Qtg9IJ<eSs04/CSA9HRSP%<Inp=MHzVY;R=}]jC' );
define( 'NONCE_KEY',        '>s_J?[fD/GZ8qlBRc7A=m1gFZ[[tMK>#&HOfL0ecWy[FoXnQX!L>_GwkVdZ?^-DW' );
define( 'AUTH_SALT',        '!tz&r_/RABpTL<hb9GObP/6PE(UaD9si@f7;=Y#GZr!6?,lG3lOyJdWE+G-9ZB?/' );
define( 'SECURE_AUTH_SALT', 'N9G+|:A{2&3qJetS`D;.1F4ZWE/CvL=}!V#KhENUKuv+yNm<&2dvzfx$wE/}7;<U' );
define( 'LOGGED_IN_SALT',   'vqz;+:d{@GOLii/w0T@c1,-XFieLHD#6CL|@E5hj*RJ,oMF?h9#[;oJDOkh6cN1~' );
define( 'NONCE_SALT',       'C 7^d@3Y!P1g8VY:D9^wB]xS@]K*m:7.BK.N4Xz9XSL|+&MD .Rwzl{{@f?&{tb ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
