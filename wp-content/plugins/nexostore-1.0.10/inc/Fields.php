<?php
/**
 * Settings Fields Loader
 * load section fields
 * @since 1.0
 */
defined( 'ABSPATH' ) || exit;

foreach( array_keys( $this->sections ) as $section ) {
    if ( @$_GET[ 'section' ] === $section ) {
        include( dirname( __FILE__ ) . '/Fields/' . $section . '.php' );
    }
}