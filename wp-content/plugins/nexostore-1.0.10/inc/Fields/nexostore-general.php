<?php
/**
 * General Settings
 */

defined( 'ABSPATH' ) || exit;

// if ( get_option( 'nexostore_installation_details', null ) === null ) {
//     return wp_redirect( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&not-authenticated=true' );
// }

/**
 * Retreive and build the 
 * shipping zone array
 */
$shippingZonesArray     =   [
    ''      =>      __( 'Disabled', 'nexostore' )
];

$rawZoneMethods         =   [];
$zonesMethods           =   [];

$rawShippingZones       =   WC_Shipping_Zones::get_zones();
foreach( $rawShippingZones as $zone ) {
    if ( intval( $zone[ 'zone_id' ] ) === intval( get_option( 'nexostore_shipping_zone' ) ) ) {
        $rawZoneMethods    =   $zone[ 'shipping_methods' ];
    }
    $shippingZonesArray[ $zone[ 'zone_id' ] ]    =   $zone[ 'zone_name' ];
}

/**
 * building zones methods
 */
foreach( $rawZoneMethods as $method ) {
    $zonesMethods[ $method->id . ':' . $method->instance_id ]   =   $method->title;
}

/**
 * witch method should be used for both options
 */
$methodsRealName        =   [
    'dinein_method'     =>  __( 'Dine in Method', 'nexostore' ),
    'takeaway_method'     =>  __( 'Pickup Method', 'nexostore' ),
    'delivery_method'   =>  __( 'Delivery Method', 'nexostore' ),
];

foreach([ 'dinein_method', 'takeaway_method', 'delivery_method' ] as $method ) {
    $$method    =   array(
        'title'         =>      sprintf( __( '%s', 'nexostore' ), $methodsRealName[ $method ] ),
        'type'          =>      'select',
        'id'            =>      'nexostore_' . $method,
        'options'       =>      $zonesMethods,
        'text'          =>      sprintf( __( '%s', 'nexostore' ), $methodsRealName[ $method ] ),
        'desc'          =>      __( 'This method will be used one the user select the matching order type on the cart.', 'nexostore' ),
        'desc_tip'      =>      true
    );
}


$fields[ $section ]     =   array(
    
    array(
        'title' => __( 'Basic Settings', 'nexostore' ),
        'type'  => 'title',
        'id'    => 'nexostore_mode_options',
    ),

    array(
        'title'         => __( 'Type', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_setup_mode',
        'options'       =>  [
            'nexopos'   =>  __( 'Only NexoPOS', 'nexostore' ),
            'gastro'    =>  __( 'NexoPOS & Gastro', 'nexostore' ),
        ],
        'text'          =>  __( 'Mode', 'nexostore' ), 
        'desc'          =>  __( 'Let you define if you want to use Nexo Store with NexoPOS only or with NexoPOS & Gastro. Useful to hide unuseful features.', 'nexostore' ),
        'desc_tip'      =>  true,
        'default'       =>  'yes'
    ),

    array(
        'type' => 'sectionend',
        'id'   => 'nexostore_mode_options',
    ),

    array(
        'title' => __( 'Sync Settings', 'nexostore' ),
        'type'  => 'title',
        'id'    => 'syncto_nexopos_options',
    ),
    
    array(
        'title'         => __( 'Allow Blank SKU', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_sync_allow_blank_sku',
        'options'       =>  [
            'no'    =>  __( 'No', 'nexostore' ),
            'yes'   =>  __( 'Yes', 'nexostore' ),
        ],
        'text'          =>  __( 'Allow Blank SKU', 'nexostore' ), 
        'desc'   =>  __( 'All products with blank SKU will have that field automatically populated by NexoPOS. 
        That might leed to multiple same entry on NexoPOS, everytime a WooCommerce product is edited.', 'nexostore' ),
        'desc_tip'  =>  true,
        'default'   =>  'yes'
    ),

    array(
        'title'         => __( 'Accept Unpaid Orders', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_sync_allow_unpaid_order',
        'options'       =>  [
            'no'    =>  __( 'No', 'nexostore' ),
            'yes'   =>  __( 'Yes', 'nexostore' ),
        ],
        'text'          =>  __( 'Accept Unpaid Orders', 'nexostore' ), 
        'desc'   =>  __( 'You might require an order to be paid before receiving that on NexoPOS.', 'nexostore' ),
        'desc_tip'  =>  true,
        'default'   =>  'yes'
    ),

    array(
        'title'         => __( 'Create new customers', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_sync_create_new_customers',
        'options'       =>  [
            'no'    =>  __( 'No', 'nexostore' ),
            'yes'   =>  __( 'Yes', 'nexostore' ),
        ],
        'text'          =>  __( 'Create the customer', 'nexostore' ), 
        'desc'   =>  __( 'If a customer doesn\'t exist on NexoPOS, you can choose to create the customer, otherwise the order will be assigned to the walking customer.', 'nexostore' ),
        'desc_tip'  =>  true,
        'default'   =>  'yes'
    ),

 array(
        'title'         => __( 'Nexo Store URL Details', 'nexostore' ),
        'type'          => 'text',
        'id'            => 'nexostore_installation_details',
       
        'text'          =>  __( 'Nexo Store URL Details', 'nexostore' ), 
        'desc_tip'  =>  true,
        'default'   =>  'yes'
    ),
    array(
        'type' => 'sectionend',
        'id'   => 'syncto_nexopos_options',
    ),
    
    array(
        'title' => __( 'Order Type', 'nexostore' ),
        'type'  => 'title',
        'id'    => 'nexostore_order_type_setttings',
    ),    

    array(
        'type' => 'sectionend',
        'id'   => 'nexostore_order_type_setttings',
    ),

    array(
        'title' => __( 'Restaurant Settings', 'nexostore' ),
        'type'  => 'title',
        'id'    => 'nexostore_restaurant_setttings',
    ),
    
   
      array(
        'title'         => __( 'Multiple Modifiers By Default', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_multiple_modifiers_by_default',
        'options'       =>  [
            'no'    =>  __( 'No', 'nexostore' ),
            'yes'   =>  __( 'Yes', 'nexostore' ),
        ],
        'text'          =>  __( 'Multiple Modifiers By Default', 'nexostore' ), 
        'desc'   =>  __( 'While creating modifiers, you might require a modifier group to only have a single modifier selected by default.', 'nexostore' ),
        'desc_tip'  =>  true,
        'default'   =>  'yes'
    ),

    array(
        'title'         => __( 'Shipping Zone', 'nexostore' ),
        'type'          => 'select',
        'id'            => 'nexostore_shipping_zone',
        'options'       =>  $shippingZonesArray,
        'text'          =>  __( 'Shipping Zone', 'nexostore' ), 
        'desc'   =>  __( 'We\'re using the shipping zone to define a free off charge fee while the customer book a table or a takeaway(take away) order.', 'nexostore' ),
        'desc_tip'  =>  true
    ),

    $dinein_method,
    $takeaway_method,
    $delivery_method,

    array(
        'type' => 'sectionend',
        'id'   => 'nexostore_restaurant_setttings',
    ),

);