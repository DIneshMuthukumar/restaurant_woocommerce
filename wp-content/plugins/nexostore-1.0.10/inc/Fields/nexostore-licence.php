<?php
/**
 * General Settings
 */
use NexoStore\Classes\Helper;

defined( 'ABSPATH' ) || exit;

/**
 * This settings appear only if the store connexion doesn't exists
 * we should consider displaying another button to revoke the connection.
 */

if ( get_option( 'nexostore_access_token' ) === false ) {

    $fields[ $section ]     =   array(
    
        array(
            'title' => __( 'Authenticate & Licence', 'nexostore' ),
            'type'  => 'title',
            'id'    => 'authentication',
        ),
    
        array(
            'title'         => __( 'Authenticate', 'nexostore' ),
            'type'          => 'link',
            'href'          =>  Helper::get_link( 'nexopos_platform_auth' ),
            'value'         =>  'nexopos_platform_auth',
            'text'          =>  __( 'Login with Nexo Platform', 'nexostore' ),
            'description'   =>  __( 'Login with NexoPlatform will ensure the syncing between NexoPOS & your WooCommerce Store.', 'nexostore' )
        ),
    
        array(
            'type' => 'sectionend',
            'id'   => 'authentication',
        ),
    
    );

} else {

    $fields[ $section ]         =   [];
    $fields[ $section ][]       =   array(
        'title' => __( 'Authenticate & Licence', 'nexostore' ),
        'type'  => 'title',
        'id'    => 'authentication',
    );
    
    if ( get_option( 'nexostore_installation_details', null ) === null ) {
        
        /**
         * we assume 16195010 is the ID of NexoPOS installation.
         */
        $result                     =   Helper::get( Helper::serverUrl( 'api/nexopos/envato-licences/16195010' ) );  

        if( $result instanceof \Exception ) {
            /**
             * the users doesn't have an account, let's him know how he can create one.
             */
            $fields[ $section ][]     =   array(
                'description'   => sprintf( __( 'An error has occured while accessing to the platform : %s', 'nexostore' ), $result->getMessage() ),
                'type'          => 'paragraph',
                'id'            => 'paragraph_field',
            );

        } else {
            
            $rawWebsites    =   json_decode( $result->body );

            /**
             * some website has been found on the customer
             * account
             */
            if( is_array( $rawWebsites ) && count( $rawWebsites ) > 0 ) {
                $options            =   [];
                foreach( $rawWebsites as $website ) {
                    $options[ $website->id ]    =   $website->app_name . ' &mdash; ' . $website->url;
                }

                /**
                 * @todo hide if the installation has been validated
                 */

                $fields[ $section ][]     =   array(
                    'title'         => __( 'Select the installation', 'nexostore' ),
                    'type'          => 'site_select',
                    'options'       =>  $options,
                    'id'            => 'nexostore_remote_websites',
                    'text'          =>  __( 'Remote Gastro', 'nexostore' ),
                    'desc'          =>  __( 'With which installation would you like to connect to. That installation should have Gastro installed on it and must be hosted on a remote server.', 'nexostore' )
                );

                $fields[ $section ][]     =   array(
                    'title'         => __( 'Select the installation', 'nexostore' ),
                    'type'          => 'hidden_field',
                    'id'            => 'rest_apis',
                    'value'         =>  json_encode( $rawWebsites )
                );
            } else {
                if ( isset( $rawWebsites->status ) ) {
                    /**
                     * the users doesn't have an account, let's him know how he can create one.
                     */
                    $fields[ $section ][]     =   array(
                        'description'   => sprintf( __( 'Nexo Platform has returned the following error :<br><strong>%s</strong>.', 'nexostore' ), esc_html( $rawWebsites->message ) ),
                        'type'          => 'paragraph',
                        'id'            => 'paragraph_field',
                    );
                } else {
                    /**
                     * the users doesn't have an account, let's him know how he can create one.
                     */
                    $fields[ $section ][]     =   array(
                        'description'   => sprintf( __( 'It seems like you don\'t have any installation linked to your Nexo Platform account. Please consider <a href="%s">adding a website</a>.', 'nexostore' ), esc_url( Helper::serverUrl( 'dashboard/profile/envato-licences'  ) ) ),
                        'type'          => 'paragraph',
                        'id'            => 'paragraph_field',
                    );
                }
            }
        }
    }
    
    $fields[ $section ][]   =   array(
        'title'         => __( 'Revoque', 'nexostore' ),
        'type'          => 'link',
        'id'            => 'nexopos_goto',
        'href'          =>  Helper::get_link( 'nexopos_platform_logout' ),
        'text'          =>  __( 'Revoke the connexion', 'nexostore' ),
        'description'   =>  __( 'Closing an existing connection.', 'nexostore' )
    );

    $fields[ $section ][]    =   array(
        'type' => 'sectionend',
        'id'   => 'authentication',
    );
}