<?php
/*
Plugin Name: Nexo Store
Plugin URI: https://nexopos.com/documentation/getting-started-with-nexo-store/
Description: Nexo Store is a plugin which ensure the syncing between WooCommerce and NexoPOS(Gastro), so that all orders, customers, items & categories are reflected on both sides.
Version: 1.0.10
Author: Nexo Solutions
Author URI: https://nexopos.com/documentation/getting-started-with-nexo-store/
Text Domain: nexostore
Domain Path: /languages
*/

namespace NexoStore;

defined( 'ABSPATH' ) || exit;

include( dirname( __FILE__ ) . '/constants.php' );
include( dirname( __FILE__ ) . '/autoload.php' );

/**
 * Since WooComerce is required, we would like to be 
 * able to test out if it's available and enabled.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

use NexoStore\Classes\Actions;
use NexoStore\Classes\Filters;
use NexoStore\Classes\View;

class Module {
    private $actions;
    private $filters;

    public function __construct()
    {
        /**
         * We can load the view. Since this class is used to
         * render/output content or notices.
         */
        $this->view     =   new View;

        /**
         * We might need to only register the hook if WooCommerce is active
         * otherwise let the user know why.
         */
        add_action( 'admin_notices', [ $this, 'check_woocommerce' ]);

        if ( 
            ! file_exists( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' ) ||
            ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            return false;
        } 

        $this->actions  =   new Actions;
        $this->filters  =   new Filters;
    }
    
    /**
     * Check if WooCommerce is installed and activated
     * @return void
     */
    public function check_woocommerce()
    {
        if ( ! file_exists( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' ) ) {
            $this->view->data([
                'message'   =>  sprintf( 
                    __( 'In order to use Nexo Store, you need to install WooCommerce. <a href="%s">Click here to install WooCommerce</a>.', 'nexostore' ),
                    admin_url( 'plugin-install.php?s=Woocommerce&tab=search&type=term' )
                )
            ])->notice();
        } else if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
            $this->view->data([
                'message'   =>  sprintf( 
                    __( 'In order to use Nexo Store, WooCommerce needs to be enabled. <a href="%s">Click here to enable WooCommerce</a>.', 'nexostore' ),
                    admin_url( 'plugins.php' )
                )
            ])->notice();
        }
    }
}

new Module;
