jQuery( document ).ready( () => {
    const NexoStoreVue   =   new Vue({
        el  :   '#nexostore-sync-template',
        data    :   {
            nexostore_sync_option   : 'sync_to_nexopos',
            has_started             :   false,
            promises                :   [],
            current_index           :   0,
            promiseQueue            :   new QueablePromises(),
            current_process_title   :   ''
        },
        created: function() {
            this.promises       =   [
                this.resolveOptions,
                this.syncCustomers,
                this.syncCategories,
                this.syncItem
            ];
        },
        computed : {
            progress_percentage() {
                return ( ( ( this.current_index - this.count_promises ) / this.count_promises ) * 100 ) + 100;
            },
            count_promises() {
                return this.promises.length;
            },
            text_domain() {
                return NexoStoreData.textDomain;
            }
        },
        filters : {

        },
        methods : {
            pushState() {
                this.current_index++;
            },

            /**
             * Start the Sync proceess
             * @return void
             */
            launchSync() {
                swal({
                    title: NexoStoreData.textDomain.confirm_sync,
                    text: NexoStoreData.textDomain.confirm_text_sync,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: NexoStoreData.textDomain.confirm_proceed_sync
                }).then( result => {
                    if ( result.value ) {
                        if ( this.nexostore_sync_option === '' ) {
                            return swal({
                                title   :   NexoStoreData.textDomain.error_occured,
                                text    :   NexoStoreData.textDomain.sync_type_missing,
                                icon    :   'warning'
                            })
                        }

                        this.has_started    =   true;

                        /**
                         * Resolve all sync processes
                         */
                        this.promiseQueue.define( this.promises ).run().then( responses => {
                            this.has_started    =   false;
                            this.current_index  =   0;
                            this.current_process_title  =   '';
                            swal({
                                title   :   NexoStoreData.textDomain.syncSuccessTitle,
                                text    :   NexoStoreData.textDomain.syncSuccessMessage
                            });
                        }).catch( errors => {
                            this.has_started    =   false;
                            const message       =   typeof errors === 'string' ? NexoStoreData.textDomain.sync_error_occured : errors.message;
                            return swal({
                                title   :   NexoStoreData.textDomain.error_occured,
                                text    :   message,
                                icon    :   'warning'
                            })
                        })
                    }
                })
            },

            /**
             * Resolve the action, by checking
             * if the multistore is enabled or not
             * @return object Promise
             */
            resolveOptions() {
                return new Promise( ( resolve, reject ) => {

                    const bodyFormData    = new FormData();

                    bodyFormData.set( 'action', 'nexo_check_options');

                    axios({
                        method: 'post',
                        url: ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {
                        this.parseResponse( result.data ).then( options => {

                            if ( options.nexo_store === 'enabled' ) {
                                swal({
                                    title   :   NexoStoreData.textDomain.error_occured,
                                    text    :   NexoStoreData.textDomain.multistore_enabled_error
                                });
                                this.resetProgress();
                                return reject( false );
                            }
                            
                            this.current_process_title  =   NexoStoreData.textDomain.title_check_options;
                            this.pushState();
                            return resolve( options );

                        }).catch( response => {
                            if ( response.status === 'failed' && response.message === 'access_denied' ) {
                                swal({
                                    title   :   NexoStoreData.textDomain.error_occured,
                                    text    :   NexoStoreData.textDomain.access_denied
                                });
                            }
                            return reject( false );
                        })
                    });
                })
            },

            /**
             * Reset the progress and so that
             * the sync proceess can be restarted
             */
            resetProgress() {
                this.has_started    =   false;
                this.current_index  =   0;
            },

            /**
             * Sync customers
             * @return object promise
             */
            syncCustomers() {
                return new Promise( ( resolve, reject ) => {
                    const bodyFormData    = new FormData();

                    bodyFormData.set( 'action', 'nexostore_sync_customers');
                    bodyFormData.set( 'sync_option', this.nexostore_sync_option );
                    bodyFormData.set( 'sync_author', jQuery( '.users-list' ).val() );

                    this.current_process_title      =   NexoStoreData.textDomain.sync_customers_title;

                    axios({
                        method: 'post',
                        url: ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {
                        this.pushState();
                        resolve( true );
                    }).catch( error => {
                        reject( error.response.data );
                    })
                })
            },

            /**
             * Sync the categories
             * @return object Promise
             */
            syncCategories() {
                return new Promise( ( resolve, reject ) => {
                    const bodyFormData    = new FormData();

                    bodyFormData.set( 'action', 'nexostore_sync_categories');
                    bodyFormData.set( 'sync_option', this.nexostore_sync_option );
                    bodyFormData.set( 'sync_author', jQuery( '.users-list' ).val() );

                    this.current_process_title  =   NexoStoreData.textDomain.title_categories;

                    axios({
                        method: 'post',
                        url: ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {
                        this.pushState();
                        resolve( true );
                    });
                })
            },
            
            /**
             * Sync the items
             * @return object Promise
             */
            syncItem() {
                return new Promise( ( resolve, reject ) => {
                    const bodyFormData    = new FormData();

                    bodyFormData.set( 'action', 'nexostore_sync_items');
                    bodyFormData.set( 'sync_option', this.nexostore_sync_option );
                    bodyFormData.set( 'sync_author', jQuery( '.users-list' ).val() );
                    
                    this.current_process_title  =   NexoStoreData.textDomain.title_items;

                    axios({
                        method: 'post',
                        url: ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {
                        this.pushState();
                        resolve( true );
                    });
                })
            },

            /**
             * parse Exceptions
             * @param object
             * @return Promise response
             */
            parseResponse( response ) {
                return new Promise( ( resolve, reject ) => {
                    if ( response.status === 'failed' ) {
                        reject( response );
                    } else {
                        resolve( response );
                    }
                });
            }
        }
    });
})