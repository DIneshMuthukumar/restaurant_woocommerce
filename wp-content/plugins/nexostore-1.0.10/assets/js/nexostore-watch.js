const NexoStoreWatch        = new function() {
    this.serverRawDateTime  = '';
    this.serverMomentTime   = null;

    this.__build            =   function( rawDateTime ) {
        this.rawDateTime    =   rawDateTime;
        this.serverMomentTime   =   moment( rawDateTime );
        this.__launchCounter();
    }

    this.__launchCounter    =   function() {
        setInterval( () => {
            this.serverMomentTime   =   this.serverMomentTime.add( 1, 's' );
        }, 1000 );
    }

    /**
     * Get the current server format
     * @param {string} get the current date format 
     * @return string;
     */
    this.getFormat          =   function( format ) {
        if ( format === undefined ) {
            return this.serverMomentTime.format();
        }
        return this.serverMomentTime.format( format );
    }

    /**
     * return the moment now
     * @return moment
     */
    this.getMoment          =   function() {
        return this.serverMomentTime;
    }

    /**
     * Build the datetime
     */
    this.__build( NexoStoreWatchData.server_time );
}