function QueablePromises() {
    this.currentIndex   =   0;
    this.allPromises    =   [];
    this.responses      =   [];

    this.define           =   ( promises ) => {
        this.currentIndex   =   0;
        this.allPromises    =   promises
        return this;
    }

    this.run            =   () => {
        return new Promise( ( resolve, reject ) => {
            if ( typeof this.allPromises[ this.currentIndex ] === 'function' ) {
                this.allPromises[ this.currentIndex ]().then( response => {
                    
                    this.responses.push( response );
                    this.currentIndex++;

                    this.run().then( response => {
                        resolve( response );
                    }).catch( error => {
                        reject( error );
                    });

                }).catch( error => {
                    reject( error );
                })
            } else {
                resolve( this.responses );
            }
        })
    }
}

// let Queue   =   new QueablePromises();
// let BasicPromise    =   () => {
//     return new Promise( ( resolve, reject ) => {
//         setTimeout( () => {
//             console.log( 'has resolved' );
//             resolve( true );
//         }, 2000 );
//     })
// };

// Queue.define([ BasicPromise, BasicPromise, BasicPromise, BasicPromise ]).run().then( response => {
//     console.log( response );
// }).catch( error => {
//     console.log( error );
// })