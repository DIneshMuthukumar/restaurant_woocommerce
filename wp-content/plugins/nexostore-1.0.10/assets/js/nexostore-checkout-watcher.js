jQuery( document ).ready( function( $ ) {

    $( '#nexostore-order-type' ).insertBefore( $( '.entry-content > .woocommerce' ) );

    const NexoStoreCheckoutVue  =   new Vue({
        el : '#nexostore-order-type',
        data: Object.assign( NexoStoreCheckoutVueData, {
            selectedOrderType: {},
            selectedTable: {},
            selectedSeats: 0,
            tables: [],
            hasLoadedTables: false,
            hasServerError: false,
            serverErrorMessage: '',
            selectedDateTime : false
        }),
        mounted() {
            $( '.lds-roller' ).hide();
            $( '#nexostore-order-type' ).show();
            $( '.entry-content > .woocommerce' ).hide();
        },
        methods: {
            /**
             * Select an ordertype
             * @param {object} orderType 
             */
            selectOrderType( orderType ) {

                if ([ 'dinein', 'takeaway', 'delivery' ].indexOf( orderType.namespace ) !== -1 ) {
                    /**
                     * Let's define the default
                     * restaurant zone which should actually be the actual
                     * store location.
                     */
                    $( '[name="calc_shipping_country"]' ).val( this.default_restaurant_zone );
                    $( '[name="calc_shipping"]' ).trigger( 'click' );
                }

                this.selectedOrderType    =   orderType;
            },

            /**
             * Update cart details
             * @return Promise<HttpResponse>
             */
            updateCartDetails() {

                var bodyFormData        =   new FormData();    
                bodyFormData.set( 'action', 'nexostore_save_order_metas');

                const data              =   $( '.woocommerce-cart-form' ).serializeArray();
                data.forEach( field => {
                    bodyFormData.set( field.name, field.value );
                });
                
               /// alert(this.ajaxurl);
                //return false;
                return axios({
                    method: 'post',
                    url: this.ajaxurl,
                    data: bodyFormData,
                    config: { headers: { 'Content-Type': 'multipart/form-data' } }
                }).then( result => {
                    console.log( result.data );
                }).catch( error => {

                });
            },

            /**
             * Cancel order type selection
             * @return void
             */
            cancelOrderType() {
                this.selectedOrderType  =   {};
                this.selectedTable      =   {};
                this.selectedSeats      =   0;
                this.selectedDateTime   =   false;
            },

            /**
             * Select the table.
             * This should show the places available selection
             * @return void
             */
            selectTableAndSeat( table, seat ) {
                this.selectedTable      =   table;
                this.selectedSeats      =   parseInt( seat );
            },

            /**
             * Show seat for a specific table
             * @return void
             */
            showSeats( event ) {
                let dropDown    =   $( event.target ).next( '.dropdown-menu' );
                if ( $( dropDown ).attr( 'is-shown' ) !== 'true' ) {
                    $( dropDown ).show();
                    $( dropDown ).attr( 'is-shown', 'true' );
                } else {
                    $( dropDown ).hide();
                    $( dropDown ).attr( 'is-shown', 'false' );
                }
            },

            /**
             * Get Seat Array
             * @return array of empty places
             */
            getSeatArray( table ) {
                return new Array( parseInt( table.MAX_SEATS ) );
            },

            /**
             * Build Hidden Fields
             * @return void
             */
            buildHidden() {
                /**
                 * If the date is provided, let's fill it
                 * or leave it empty.
                 */
                let dateValue   =   '';
                if ( this.selectedDateTime !== false ) {
                    dateValue   =   this.selectedDateTime.format();
                }

                $( '#nexostore-hidden-fields' ).remove();
                $( '.woocommerce-cart-form' ).append( `<div id="nexostore-hidden-fields"></div>` );
                $( '#nexostore-hidden-fields' ).append( `
                <input type="hidden" name="nexostore_order_type" value="${this.selectedOrderType.namespace}"/>
                <input type="hidden" name="nexostore_table_id" value="${this.selectedTable.TABLE_ID || 0}"/>
                <input type="hidden" name="nexostore_table_seats" value="${this.selectedSeats}"/>                
                <input type="hidden" name="nexostore_datetime" value="${dateValue}"/>
                ` )

            },

            /**
             * Parse response to provide a proper 
             * answer according to what the server return
             * @return object
             */
            parseResponse( result ) {
                if ( result.data.status !== undefined && result.data.status === 'failed' ) {
                    this.hasServerError         =   true;
                    this.serverErrorMessage     =   result.data.message;
                    return false;
                }
                this.hasServerError     =   false;
                return result;
            },

            /**
             * Select a DateTime
             * @return void
             */
            selectDateTime() {
                /**
                 * the datetime has'nt been selected
                 * we should ask to confirm that
                 */

                this.selectedDateTime   =   $( '#nexostore-datetime' ).datetimepicker( 'viewDate' );
            },

            /**
             * Define Shipping Option
             * This method choose wether the customer should have a free
             * shipping if the dinein is selected for example
             * @param fields
             * @return void
             */
            defineShippingOption( fields ) {
                let that    =   this;
                $( fields ).each( function() {
                    switch( that.selectedOrderType.namespace ) {
                        case 'dinein': 
                            if ( ! $( this ).is( ':checked' ) && $( this ).val() === that.dinein_shipping_method ) {
                                $( this ).trigger( 'click' );
                            }
                        break;
                        case 'takeaway':
                            if ( ! $( this ).is( ':checked' ) && $( this ).val() === that.takeaway_shipping_method ) {
                                $( this ).trigger( 'click' );
                            }
                        break;
                        case 'delivery':
                            if ( ! $( this ).is( ':checked' ) && $( this ).val() === that.delivery_shipping_method ) {
                                $( this ).trigger( 'click' );
                            }
                        break;
                    }
                });
            }
        },
        watch: {
            showCheckout() {
                if ( this.showCheckout ) {
                    this.buildHidden();
                    this.updateCartDetails();
                    $( '.entry-content > .woocommerce' ).show();
                    return;
                }
                $( '.entry-content > .woocommerce' ).hide();
            },

            showTables() {
                if ( this.showTables ) {
                    
                    this.tables             =   [];
                    this.hasLoadedTables    =   false;
                    this.hasServerError     =   false;
                    
                    var bodyFormData        =   new FormData();    
                    bodyFormData.set( 'action', 'nexostore_available_tables');                    

                    axios({
                        method: 'post',
                        url: this.ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {

                        /**
                         * let's parse the response first
                         */
                        if ( ( result  =   this.parseResponse( result ) ) !== false ) {
                            let availableTables     =   result.data.filter( table => table.STATUS === 'available' );
                            this.tables             =   availableTables;
                            this.hasLoadedTables    =   true;
                            return;
                        }

                        /**
                         * What to do in case of error
                         */
                        this.cancelOrderType();

                    }).catch( error => {
                        this.hasAvailableTables     =   true;
                        this.hasServerError         =   true;
                        this.tables                 =   [];
                        console.log( error );
                    });
                }
            }
        },
        computed: {
            /**
             * Check if it has selected the order Type
             */
            showOrderTypes() {
                return Object.values( this.selectedOrderType ).length === 0;
            },

            /**
             * Check if it has selected the table
             */
            hasSelectedTable() {
                return Object.values( this.selectedTable ).length > 0;
            },

            /**
             * Check if it has selected the seats
             */
            hasSelectedSeats() {
                return this.selectedSeats !== 0;
            },

            /**
             * Has selected Datetime
             * @return boolean
             */
            hasSelectedDateTime() {
                return this.selectedDateTime !== false;
            },

            /**
             * Define whether the current oder is a dine
             * in order
             * @return boolean
             */
            isDineInOrder() {
                return this.selectedOrderType.namespace === 'dinein';
            },

            /**
             * Define whether the current oder is a takeaway order
             * @return boolean
             */
            isTakeAwayOrder() {
                return this.selectedOrderType.namespace === 'takeaway';
            },

            /**
             * Define when checkout should be shown
             */
            showCheckout() {
                return (
                    (
                        this.isDineInOrder &&
                        this.hasSelectedTable &&
                        this.hasSelectedSeats &&
                        this.hasSelectedDateTime &&
                        ! this.showOrderTypes
                    ) || (
                        this.hasSelectedDateTime &&
                        ! this.isDineInOrder &&
                        ! this.showOrderTypes
                    )
                )
            },

            /**
             * Define when table should be loaded
             * @return boolean
             */
            showTables() {
                return this.isDineInOrder && 
                    ! this.hasSelectedTable && 
                    ! this.hasSelectedSeats && 
                    ! this.hasSelectedDateTime &&
                    ! this.showOrderTypes;
            },

            /**
             * Show seats
             * @return boolean
             */
            showTableSeats() {
                return this.isDineInOrder && 
                    this.hasSelectedTable && 
                    ! this.hasSelectedSeats && 
                    ! this.hasSelectedDateTime && 
                    ! this.showOrderTypes;
            },

            /**
             * Wether the datetime picker should
             * be shown
             * @return boolean
             */
            showDateTime() {

                /**
                 * Right after Vue has rendered the vue
                 */
                setTimeout( () => {
                    $( '#nexostore-datetime' ).datetimepicker( 'destroy' );
                    $( '#nexostore-datetime' ).datetimepicker({
                        inline: true,
                        sideBySide: true,
                        minDate: NexoStoreWatch.getMoment()
                    });
                }, 200 );

                return (
                    ( 
                        this.isDineInOrder && 
                        this.hasSelectedTable && 
                        this.hasSelectedSeats
                    ) || (
                        ! this.isDineInOrder
                    ) 
                ) && (
                    ! this.hasSelectedDateTime && 
                    ! this.showOrderTypes
                );
            },

            /**
             * Has available tables
             * @return boolean
             */
            hasAvailableTables() {
                return this.tables.length > 0;
            }
        }
    });

    // let timeoutWatcher;
    // $( document ).ajaxComplete( () => {
    //     if ( timeoutWatcher === undefined ) {
    //         timeoutWatcher  =   setInterval( () => {
                

    //         }, 1000 );
    //     }
    //     console.log( NexoStoreCheckoutVue.cartRefreshStatus );

    //     /**
    //      * if the cash refresh status is ongoing
    //      * that means we've updated the cart from
    //      * NexoStoreCheckoutVue. Now let's restore the default 
    //      * state
    //      */
    //     if ( NexoStoreCheckoutVue.cartRefreshStatus === 'ongoing' ) {
    //         NexoStoreCheckoutVue.cartRefreshStatus  =   'pending';
    //     }

    //     console.log( NexoStoreCheckoutVue.cartRefreshStatus );
    // });

    $( document ).on( 'updated_cart_totals', function( ) {
        if( 
            $( '[name="shipping_method[0]"]' ).length > 0
        ) {
            NexoStoreCheckoutVue.defineShippingOption( $( '[name="shipping_method[0]"]' ) );
        }
    })
})
