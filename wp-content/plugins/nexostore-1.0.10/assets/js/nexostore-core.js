/**
 * Class Holder for NexoStore API
 */
function NexoStoreClass () {
    const $             =   jQuery;

    /**
     * Ping the website to compare the required version 
     * of NexoPOS & Gastro.
     */
    this.ping   =   () => {
        var bodyFormData    = new FormData();

        bodyFormData.set('action', 'nexostore_ping_platform_gastro');
        bodyFormData.set('installation_id', $( '[name="nexostore_remote_websites"]' ).val());

        /**
         * Ping the website
         */
        axios({
            method: 'post',
            url: this.ajaxurl ? this.ajaxurl : ajaxurl,
            data: bodyFormData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        })
        .then( ( response ) => {

            if ( response.data.gastro === undefined ) {
                $( '.nexostore-notice' ).remove();
                $( '#wpbody-content form > h1.screen-reader-text' ).after( `
                    <div class="notice notice-error is-dismissible nexostore-notice">
                        <p>${response.data.message || NexoStoreData.textDomain.gastro_missing}</p>
                    <button type="button" class="notice-dismiss"><span class="screen-reader-text">${NexoStoreData.textDomain.skip}</span></button></div>
                ` );

                this.__bindRemoveNotice();

                return false;
            }
            const gastro    =   response.data.gastro.application;

            if ( response.data.nexo === undefined ) {
                $( '.nexostore-notice' ).remove();
                $( '#wpbody-content form > h1.screen-reader-text' ).after( `
                    <div class="notice notice-error is-dismissible nexostore-notice">
                        <p>${NexoStoreData.textDomain.gastro_missing}</p>
                    <button type="button" class="notice-dismiss"><span class="screen-reader-text">${NexoStoreData.textDomain.skip}</span></button></div>
                ` );
                
                this.__bindRemoveNotice();

                return false;
            }
            const nexo  =   response.data.nexo.application;

            if ( NexoStoreHelper.versionCompare( gastro.version, '<', '2.3.12' ) ) {
                $( '.nexostore-notice' ).remove();
                $( '#wpbody-content form > h1.screen-reader-text' ).after( `
                    <div class="notice notice-error is-dismissible nexostore-notice">
                        <p>${NexoStoreData.textDomain.gastro_version_issue}</p>
                    <button type="button" class="notice-dismiss"><span class="screen-reader-text">${NexoStoreData.textDomain.skip}</span></button></div>
                `);

                this.__bindRemoveNotice();

                return false;
            }

            if ( NexoStoreHelper.versionCompare( nexo.version, '<', '3.13.6' ) ) {
                $( '.nexostore-notice' ).remove();
                $( '#wpbody-content form > h1.screen-reader-text' ).after( `
                    <div class="notice notice-error is-dismissible nexostore-notice">
                        <p>${NexoStoreData.textDomain.nexopos_version_issue}</p>
                    <button type="button" class="notice-dismiss"><span class="screen-reader-text">${NexoStoreData.textDomain.skip}</span></button></div>
                `);

                this.__bindRemoveNotice();

                return false;
            }

            $( '.nexostore-notice' ).remove();
            $( '#wpbody-content form > h1.screen-reader-text' ).after( `
                <div class="notice notice-success is-dismissible nexostore-notice">
                    <p>${NexoStoreData.textDomain.nexopos_success}</p>
                <button type="button" class="notice-dismiss"><span class="screen-reader-text">${NexoStoreData.textDomain.skip}</span></button></div>
            `);

            this.__bindRemoveNotice();

            /**
             * should submit a post request to validate installation.
             */
            this.saveWebsite( response.data );
        })
        .catch(function (response) {
            console.log( response );
            alert( `${NexoStoreData.textDomain.ping_error}` );
        });
    }

    /**
     * Save website once it has been 
     * successfully pinged
     * @return void
     */
    this.saveWebsite            =   function( modules ){
        const apis              =   JSON.parse( $( '#rest_apis' ).val() );
        let installation_data   =   apis.filter( api => {
            if ( api.id === parseInt( $( '[name="nexostore_remote_websites"]' ).val() ) ) {
                return true;
            }
            return false;
        })[0]; // => grab the details

        const bodyFormData    = new FormData();

        bodyFormData.set( 'action', 'nexostore_save_installation');
        bodyFormData.set( 'installation_id', $( '[name="nexostore_remote_websites"]' ).val());
        bodyFormData.set( 'installation_data', JSON.stringify( installation_data ) );
        bodyFormData.set( 'nexopos', JSON.stringify( modules.nexo.application ) );

        axios({
            method: 'post',
            url: this.ajaxurl ? this.ajaxurl : ajaxurl,
            data: bodyFormData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}
        }).then( result => {
            if ( result.data.status == 'success' ) {
                location.reload();
            }
        });
    }

    /**
     * Bind Remove notice
     * @return void
     */
    this.__bindRemoveNotice     =   function() {
        $( '.notice-dismiss' ).bind( 'click', function() {
            $( this ).closest( '.notice' ).fadeOut(500, function() {
                $( this ).remove();
            });
        })
    }
}

/**
 * Loading the core
 */
var NexoStoreCore;

jQuery( document ).ready( function( $ ) {
    NexoStoreCore   =   new NexoStoreClass();
});