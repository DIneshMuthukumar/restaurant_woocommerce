const NexoStoreHelper =   new function() {
    this.versionCompare = function(left, comparison = '>', right ) {
        if (typeof left + typeof right != 'stringstring')
            return false;
        
        var a = left.split('.')
        ,   b = right.split('.')
        ,   i = 0, len = Math.max(a.length, b.length);
            
        for (; i < len; i++) {
            if ((a[i] && !b[i] && parseInt(a[i]) > 0) || (parseInt(a[i]) > parseInt(b[i]))) {
                switch( comparison ) {
                    case '>' : return true;
                    case '<' : return false;
                }
            } else if ((b[i] && !a[i] && parseInt(b[i]) > 0) || (parseInt(a[i]) < parseInt(b[i]))) {
                switch( comparison ) {
                    case '<' : return true;
                    case '>' : return false;
                }
            }
        }
        
        switch( comparison ) {
            case '>=' : return true;
            case '<=' : return true;
            case '==' : return true;
            case '>'  : return false;
            case '<'  : return false;
        }
    }
}