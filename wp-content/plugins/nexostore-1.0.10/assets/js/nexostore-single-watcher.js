jQuery( document ).ready( function($) {
    const NexoStoreSingleVue    =   new Vue({
        el: '#nexostore-single-product-modifier',
        data: Object.assign( NexoStoreSingleProductData, {
            index: 0,
            previousText: ''
        }),
        mounted() {
            /**
             * Remove the spinner 
             * and load the Vue Component
             */
            $( '.lds-roller' ).hide();
            $( '#nexostore-single-product-modifier' ).show();

            this.previousText   =   $( '[name="add-to-cart"]' ).text();
            const $this         =   this;
            
            $( '[name="add-to-cart"]' ).text( this.textDomain.chooseModifier );
            $( '[name="add-to-cart"]' ).bind( 'click', function() {
                if ( $( this ).attr( 'nexostore-can-proceed' ) === undefined ) {

                    if ( 
                        $this.groups[ $this.index ].group_forced === 'yes' &&
                        $this.groups[ $this.index ].modifiers.filter( modifier => modifier.selected === 'yes' ).length === 0
                    ) {
                        console.log( $this );
                        swal({
                            title: $this.textDomain.modifierRequired,
                            text: $this.textDomain.modifierRequiredText,
                            showCloseButton: true,
                            showCancelButton: true,
                            focusConfirm: false,
                            confirmButtonText: $this.textDomain.confirmText
                        });
                        return false;
                    }

                    $this.nextModifier();

                    return false;
                } else {
                    $( '[name="add-to-cart"]' ).text( $this.previousText );
                    
                    /**
                     * Let's create the hidden field using the modifiers
                     * groups. This will be used to add the meta to the 
                     * item on the cart
                     */
                    $this.groups.forEach( ( group, group_id ) => {

                        /**
                         * Build the hidden field for the group
                         */
                        let groupKeys   =   Object.keys( group );
                        groupKeys.forEach( key => {
                            $( '#nexostore-single-product-modifier' ).append( `
                                <input type="hidden" name="nexostoreModifiers[${group_id}][${key}]" value="${group[key]}"/>
                            ` )
                        });

                        /**
                         * Build the hidden field for the modifiers
                         */
                        group.modifiers.forEach( ( modifier, modifier_id ) => {
                            let modifierKeys    =   Object.keys( modifier );
                            modifierKeys.forEach( key => {
                                $( '#nexostore-single-product-modifier' ).append( `
                                    <input type="hidden" name="nexostoreModifiers[${group_id}][modifiers][${modifier_id}][${key}]" value="${modifier[key]}"/>
                                ` )
                            })
                        })
                    });
                    
                    return true;
                }
            })
        },
        methods: {
            toggleModifier( modifier ) {
                const index   =   this.groups[ this.index ].modifiers.indexOf( modifier );
    
                /**
                 * Let's set a all modifier as unselected
                 */
                if ( this.groups[ this.index ].group_multiselect !== 'yes' ) {
                    this.groups[ this.index ].modifiers.forEach( ( __modifier, __index ) => {
                        if ( __index !== index ) {
                            __modifier.selected  =   'no';
                        }
                    })
                }

                this.groups[ this.index ].modifiers[ index ].selected  =   modifier.selected === 'yes' ? 'no' : 'yes';
                this.$forceUpdate();
            },

            /**
             * Show the next modifier group
             * @return void
             */
            nextModifier() {
                if ( this.groups[ this.index + 1 ] === undefined ) {
                    $( '[name="add-to-cart"]' ).attr( 'nexostore-can-proceed', 'true' );
                    $( '[name="add-to-cart"]' ).trigger( 'click' );
                    return false;
                } else {
                    this.index++;
                    this.$forceUpdate();
                }
            }
        }
    })
})