jQuery( document ).ready(function( $ ) {
    const NexoStoreWatchAddToCart   =   new Vue({
        el : ".products",
        data: Object.assign( NexoStoreAddToCartData, {
            groupIndex   :   0
        }),
        mounted() {
            const $this     =   this;
            $( '.product_type_simple' ).bind( 'click', function() {

                if ( $( this ).data( 'nexopos-can-add-to-cart' ) === 'true' ) {
                    $( this ).data( 'nexopos-can-add-to-cart', 'false' );
                } else {

                    var previousText    =   $( this ).text();
                    $( this ).text( $this.textDomain.pleaseWait );
                    var bodyFormData    = new FormData();
    
                    bodyFormData.set( 'action', 'nexostore_check_modifiers');
                    bodyFormData.set( 'product_id', $( this ).data( 'product_id' ) );

                    axios({
                        method: 'post',
                        url: $this.ajaxurl,
                        data: bodyFormData,
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    }).then( result => {
                        if ( result.data.status !== undefined || result.data.lengt === 0 ) {
                            $( this ).text( previousText );
                            $( this ).data( 'nexopos-can-add-to-cart', 'true' );
                            $( this ).trigger( 'click' );
                        } else {
                            NexoStoreWatchAddToCart.handleModifiers({
                                data: result.data,
                                element: $( this ),
                                previousText
                            });
                        }
                    }).catch( error => {
                        /**
                         * Add the item to the card
                         */
                        $( this ).text( previousText );
                        // AddToCartHandler.onAddToCart( $( this ) );
                    })
    
                    return false;
                }
            })
        },
        methods: {
            /**
             * Handle Modifies
             * @param {JSON} response of the modifiers 
             * @param {JqueryObject} element clicked on
             * @param {string} text of the button
             */
            handleModifiers({ data, element, previousText }) {
                this.groupIndex     =   0;
                $( element ).text( previousText );
                this.showGroup( data ).then( groups => {
                    $( element ).data( 'nexostore-modifiers', groups );
                    $( element ).data( 'nexopos-can-add-to-cart', 'true' );
                    $( element ).trigger( 'click' );
                })
            },

            /**
             * Display a group of modifiers
             * @param {Array} groups 
             */
            showGroup( groups ) {
                return new Promise( ( resolve, reject ) => {
                    if ( groups[ this.groupIndex ] !== undefined ) {
                        let group   =   groups[ this.groupIndex ];
                        let width   =   '50%'; // default width

                        /**
                         * Find the right width for the popup
                         */
                        if ( group.modifiers.length < 4 ) {
                            width   =   '30%';
                        } else if ( group.modifiers.length < 8 ) {
                            width   =   '50%';
                        } else if ( group.modifiers.length < 12 ) {
                            width   =   '70%';
                        } else {
                            width   =   '80%';
                        }

                        swal({
                            title: group.group_name,
                            html: `
                            <div id="modifier-list">
                                <p>{{ group.group_description }}</p>
                                <div class="flexbox">
                                    <div v-for="modifier in modifiers" class="box">
                                        <div class="modifier" :class="{ 'active' : modifier.selected === 'yes' }" @click="toggleActive( modifier )">
                                            <img :src="modifier.thumbnail" class="modifier-image"/>
                                            <h4 style="margin:0">{{ modifier.name }}</h4>
                                            <p style="margin:10px; 0">{{ currency_symbol }} {{ modifier.price }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>`,
                            width
                        }).then( result => {
                            if ( result.value ) {

                                /**
                                 * Let's check first if the group require a selections
                                 * before moving ahead
                                 */
                                const currentGroup  =   groups[ this.groupIndex ];

                                if ( 
                                    currentGroup.group_forced === 'yes' &&  
                                    currentGroup.modifiers.filter( modifier => modifier.selected === 'yes' ).length === 0
                                ) {
                                    swal({
                                        title: this.textDomain.modifierRequired,
                                        text: this.textDomain.modifierRequiredText,
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        focusConfirm: false,
                                        confirmButtonText: this.textDomain.confirmText
                                    }).then( result => {
                                        if ( result.value ) {
                                            this.showGroup( groups ).then( groups => {
                                                resolve( groups );
                                            })
                                        }
                                    })
                                    return false;
                                }

                                this.groupIndex++;
                                this.showGroup( groups ).then( groups => {
                                    resolve( groups );
                                })
                            }
                        })
    
                        const ModifierGroupListVue  =   new Vue({
                            el: '#modifier-list',
                            data: Object.assign(NexoStoreAddToCartData, {
                                modifiers: group.modifiers,
                                group: group
                            }),
                            mounted() {
    
                            },
                            methods: {
                                toggleActive( modifier ) {
                                    
                                    const index   =   this.modifiers.indexOf( modifier );
    
                                    /**
                                     * Let's set a all modifier as unselected
                                     */
                                    if ( group.group_multiselect !== 'yes' ) {
                                        this.modifiers.forEach( ( __modifier, __index ) => {
                                            if ( __index !== index ) {
                                                __modifier.selected  =   'no';
                                            }
                                        })
                                    }
    
                                    this.modifiers[ index ].selected  =   modifier.selected === 'yes' ? 'no' : 'yes';
                                    this.$forceUpdate();
                                }
                            }
                        })
                    } else {
                        resolve( groups );
                    }
                })
            }
        }
    })
})