<?php
namespace NexoStore\Classes;

use Exception;
use WC_API_Customers;
use WC_Product_Simple;
use Requests_Exception;
use NexoStore\Classes\Helper;
use NexoStore\Classes\SyncResponse;
use WC_Product;

class Sync
{
    /**
     * Sync Category from WooCommerce to NexoPOS
     * @return Array as a response
     */
    public static function categoryToNexoPOS()
    {
        $get_featured_cats = array(
            'taxonomy'     => 'product_cat',
            'orderby'      => 'name',
            'hide_empty'   => false
        );

        $all_categories         =   get_categories( $get_featured_cats );
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        /**
         * Post Retreived category to NexoPOS
         * with an option to erase the categories of NexoPOS
         */
        $response  =   Helper::nexopos_post( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/woo_categories', [
                'categories'    =>  $all_categories,
                'author'        =>  intval( $_POST[ 'sync_author' ] )
            ]
        );

        return self::response( $response );
    }

    /**
     * Sync category from NexoPOS to WooCommerce
     * @return array as a Response
     */
    public static function categoryToWooCommerce()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        /**
         * Post Retreived category to NexoPOS
         * with an option to erase the categories of NexoPOS
         */
        $response  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/categories'
        );

        $result         =   [];

        foreach( json_decode( $response->body, true ) as $cat ) {
            try {
                wp_insert_term( $cat[ 'NOM' ], 'product_cat', array(
                    'description' => $cat[ 'DESCRIPTION' ], // optional
                    'slug' => Helper::slugify( $cat[ 'NOM' ] ) // optional
                ) );

                $result[]   =   [
                    'status'    =>  'success',
                    'message'   =>  __( 'The category has successfully been created.', 'nexostore' )
                ];

            } catch( Exception $e ) {
                $result[]   =   [
                    'status'    =>  'failed',
                    'message'   =>  $e->getMessage(),
                ];
            }
        }

        return self::response([
            'status'    =>  'success',
            'message'   =>  __( 'The process has been completed', 'nexostore' ),
            'data'      =>  compact( 'result' )
        ]);
    }

    /**
     * send items from NexoPOS to WooCommerce
     * @return array json response
     */
    public static function itemsToWooCommerce()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        
        /**
         * Post Retreived category to NexoPOS
         * with an option to erase the categories of NexoPOS
         */
        $response  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/items'
        );

        // echo "<pre>";
        // print_r($response);
        // die();
        $items      =   json_decode( $response->body, true );
        $result     =   [];

        if ( isset( $items[ 'status' ] ) && $items[ 'status' ] === 'failed' ) {
            return self::response([
                'status'    =>  'failed',
                'message'   =>  __( 'Unable to synchronize : ' . $items[ 'message' ], 'nexostore' ),
            ]);
        }

        global $syncBack;
        $syncBack   =   false;

        foreach( $items as $item ) {
            try {
                $product        =   Helper::get_product_by_sku( $item[ 'SKU' ] );

                $post           =   [
                    'post_author'   =>  intval( $_POST[ 'sync_author' ] ),
                    'post_content'  =>  $item[ 'DESCRIPTION' ],
                    'post_status'   =>  'draft',
                    'post_title'    =>  $item[ 'DESIGN' ],
                    'post_type'     =>  'product'
                ];

                /**
                 * if the post already exists
                 * then we'll get the existing one
                 */
                $product_id         =   null;
                if ( $product instanceof WC_Product ) {
                    unset( $post[ 'post_status' ] );
                    $endProduct     =   array_merge([
                        'ID'    =>  $product->id,
                    ], ( array ) $post );

                    wp_update_post( $endProduct );
                    $product_id     =   $product->id;
                } else {
                    $product_id     =   wp_insert_post( $post );
                }

                $term               =   get_term_by( 'name', $item[ 'category' ][ 'NOM' ], 'product_cat' );
                
                wp_set_object_terms( $product_id, $term->term_id, 'product_cat' );
                wp_set_object_terms( $product_id, 'simple', 'product_type' );

                delete_post_meta( $product_id, '_regular_price' );
                delete_post_meta( $product_id, '_price' );
                delete_post_meta( $product_id, '_sku' );
                delete_post_meta( $product_id, '_visibility' );
                delete_post_meta( $product_id, '_stock' );
                delete_post_meta( $product_id, '_alt_name' );
                delete_post_meta( $product_id, '_low_stock_amount' );
                delete_post_meta( $product_id, '_manage_stock' );

                update_post_meta( $product_id, '_regular_price', $item[ 'PRIX_DE_VENTE_TTC' ], true );
                update_post_meta( $product_id, '_price', $item[ 'PRIX_DE_VENTE_TTC' ], true );
                update_post_meta( $product_id, '_sku', $item[ 'SKU' ], true );
                update_post_meta( $product_id, '_visibility', 'visible', true );
                update_post_meta( $product_id, '_stock', $item[ 'QUANTITE_RESTANTE' ], true );
                update_post_meta( $product_id, '_alt_name', $item[ 'ALTERNATIVE_NAME' ], true );
                update_post_meta( $product_id, '_low_stock_amount', $item[ 'ALERT_QUANTITY' ], true );
                
                if ( $item[ 'STOCK_ENABLED' ] === '1' ) {
                    update_post_meta( $product_id, '_manage_stock', 'yes' );
                } else {
                    delete_post_meta( $product_id, '_manage_stock', 'yes' );
                }

                $result[]   =   [
                    'status'    =>  'success',
                    'message'   =>  $product instanceof WC_Product ? __( 'The product has been updated', 'nexostore' ) : __( 'The product has been created.', 'nexostore' ),
                    'data'      =>  compact( 'term', 'product_id', 'item' )
                ];

            } catch( Exception $e ) {
                $result[]   =   [
                    'status'    =>  'failed',
                    'message'   =>  __( 'Unable to create the product.', 'nexostore' )
                ];
            }
        }

        return self::response([
            'status'    =>  'success',
            'message'   =>  __( 'The operation was successful.' , 'nexostore' ),
            'data'      =>  compact( 'result' )
        ]);
    }

    function attach_product_thumbnail($post_id, $url, $flag){
        /*
         * If allow_url_fopen is enable in php.ini then use this
         */
        $image_url = $url;
        $url_array = explode('/',$url);
        $image_name = $url_array[count($url_array)-1];
        $image_data = file_get_contents($image_url); // Get image data
        /*
        * If allow_url_fopen is not enable in php.ini then use this
        */
        // $image_url = $url;
        // $url_array = explode('/',$url);
        // $image_name = $url_array[count($url_array)-1];
        // $ch = curl_init();
        // curl_setopt ($ch, CURLOPT_URL, $image_url);
        // // Getting binary data
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        // $image_data = curl_exec($ch);
        // curl_close($ch);
        $upload_dir = wp_upload_dir(); // Set upload folder
        $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); //    Generate unique name
        $filename = basename( $unique_file_name ); // Create image file name
        // Check folder permission and define file location
        if( wp_mkdir_p( $upload_dir['path'] ) ) {
            $file = $upload_dir['path'] . '/' . $filename;
        } else {
            $file = $upload_dir['basedir'] . '/' . $filename;
        }
        // Create the image file on the server
        file_put_contents( $file, $image_data );
        // Check image file type
        $wp_filetype = wp_check_filetype( $filename, null );
        // Set attachment data
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name( $filename ),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        // Create the attachment
        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
        // Include image.php
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        // Define attachment metadata
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        // Assign metadata to attachment
        wp_update_attachment_metadata( $attach_id, $attach_data );
        // asign to feature image
        if( $flag == 0){
            // And finally assign featured image to post
            set_post_thumbnail( $post_id, $attach_id );
        }
        // assign to the product gallery
        if( $flag == 1 ){
            // Add gallery image to product
            $attach_id_array = get_post_meta($post_id,'_product_image_gallery', true);
            $attach_id_array .= ','.$attach_id;
            update_post_meta($post_id,'_product_image_gallery',$attach_id_array);
        }
    }

    /**
     * Products to NexoPOS
     */
    public static function itemsToNexoPOS()
    {
        /**
         * Let's store skipped items
         * @param array
         */
        // $skippedItems       =   [];

        /**
         * Get Raw products
         */
        $rawProducts               =   wc_get_products([
            'status'        =>  'publish',
            'paginate'      =>  false,
            'limit'         =>  -1
        ]); 

        /**
         * Filters Product with empty SKU
         */
        $products       =   array_filter( $products, function( $product ) use ( &$skippedItems ) {
            if ( 
                ( 
                    get_option( 'nexostore_sync_allow_blank_sku', 'no' ) === 'yes' &&
                    empty( $product->get_sku() )
                ) || ! empty( $product->get_sku() )
            ) {
                return $product;
            } 
        });

        /**
         * Turn the Object in to a plain array
         */
        $products       =   array_map(function( $product ) {
            
            $categories             =   [];
            foreach( $product->get_category_ids() as $id ) {
                $cat                =   get_categories([
                    'taxonomy'      =>  'product_cat',
                    'hide_empty'    =>  true,
                    'include'       =>  [ $id ]
                ]);
                $categories[]       =   $cat[0];
            }

            return [
                'id'                =>  $product->get_ID(),
                'name'              =>  $product->get_name(),
                'description'       =>  $product->get_description(),
                'created_on'        =>  $product->get_date_created(),
                'sku'               =>  $product->get_sku(),
                'quantity'          =>  $product->get_stock_quantity(),
                'sale_price'        =>  $product->get_price(),
                'discount_price'    =>  $product->get_sale_price(),
                'discount_starts'   =>  $product->get_date_on_sale_from(),
                'discount_ends'     =>  $product->get_date_on_sale_to(),
                'stock_management'  =>  $product->get_manage_stock(),
                'type'              =>  $product->get_type(),
                'category_ids'      =>  $product->get_category_ids(),
                'categories'        =>  $categories
            ];

        }, $rawProducts );

        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        /**
         * Post Retreived category to NexoPOS
         * with an option to erase the categories of NexoPOS
         */
        $response  =   Helper::nexopos_post( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/woo_products', [
                'products'      =>  $products,
                'author'        =>  intval( $_GET[ 'sync_author' ] )
            ]
        );

        return self::response( $response );
    }

    /**
     * Sync Options from WooCommerce to NexoPOS
     * @return json as Response
     */
    public static function settingsToNexoPOS()
    {
        global $nexo_store_currencies;

        $currency_iso           =   get_option( 'woocommerce_currency' );
        $currency_symbol        =   isset( $nexo_store_currencies[ $currency_iso ] ) ? $nexo_store_currencies[ $currency_iso ] : $currency_iso;
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        $response  =   Helper::nexopos_post( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/options', [
                'options'   =>  [
                    'nexo_currency_iso'     =>  $currency_iso,
                    'nexo_currency'         =>  $currency_symbol,
                    'nexo_shop_address_1'   =>  get_option( 'woocommerce_store_address' ),
                    'nexo_shop_address_2'   =>  get_option( 'woocommerce_store_address_2' ),
                    'nexo_shop_city'        =>  get_option( 'woocommerce_store_city' ),
                    'nexo_shop_pobox'       =>  get_option( 'woocommerce_store_postcode' )
                ]
            ]
        );

        return self::response( $response );
    }

    /**
     * Check option update
     * if options saved are from
     * @return json
     */
    public static function checkOptionUpdate( $option, $old, $new )
    {
        /**
         * if all options saved are from woocommerce (hopefully), let's 
         * sync the options
         */
        if ( substr( $option, 0, 11 ) === 'woocommerce' ) {
            self::settingsToNexoPOS();
        }
    }

    /**
     * Create Product
     * @return json
     */
    public static function createProduct( $product_id, $post, $update )
    {
        global $NexoStoreGlobal, $syncBack;

        /**
         * We don't want to create a loop synchronization
         */
        if ( $syncBack === false ) {
            return false;
        }
        
        if ( $update ) {
            $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
            $product                =   wc_get_product( $product_id );  
            $categories             =   [];

            if ( $product !== false ) {

                foreach( $product->get_category_ids() as $id ) {
                    $cat                =   get_categories([
                        'taxonomy'      =>  'product_cat',
                        'include'       =>  [ $id ]
                    ]);
                    $categories[]       =   $cat[0];
                }

                /**
                 * Check if the blank entry is allowed
                 * or return an error
                 */
                if ( 
                    get_option( 'nexostore_sync_allow_blank_sku', 'no' ) === 'no' &&
                    empty( $product->get_sku() )
                ) {
                    return [
                        'status'    =>  'failed',
                        'message'   =>  sprintf( __( 'Unable send a product %s to NexoPOS since it has an empty SKU value', 'nexostore' ), $product->get_name() )
                    ];
                }    

                $response  =   Helper::nexopos_post( 
                    $installationDetails[ 'rest_key' ],
                    rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/woo_product', [
                        'product'   =>  [
                            'id'                =>  $product_id,
                            'quantity'          =>  $product->get_stock_quantity(),
                            'name'              =>  $product->get_name(),
                            'description'       =>  $product->get_description(),
                            'created_on'        =>  $product->get_date_created(),
                            'sku'               =>  $product->get_sku(),
                            'sale_price'        =>  $product->get_price(),
                            'discount_price'    =>  $product->get_sale_price(),
                            'discount_starts'   =>  $product->get_date_on_sale_from(),
                            'discount_ends'     =>  $product->get_date_on_sale_to(),
                            'stock_management'  =>  $product->get_manage_stock(),
                            'type'              =>  $product->get_type(),
                            'category_ids'      =>  $product->get_category_ids(),
                            'old_sku'           =>  @$NexoStoreGlobal[ 'sku' ]
                        ],
                        'categories'  =>  $categories
                    ]
                );
                
                return self::response( $response );
            }
        }
    }

    /**
     * Delete Product to NexoPOS
     * @return json
     */
    public static function deleteProduct( WC_Product_Simple $product )
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        $response  =   Helper::nexopos_delete( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/woo_product/?sku=' . $product->get_sku()
        );

        return self::response( $response );
    }

    /**
     * Order To NexoPOS
     * Submit an order to nexopos once it's placed
     * @return json
     */
    public static function orderToNexoPOS( $details )
    {

        // echo "<pre>";
        // print_r($details);
        // die();
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        
        $response  =   Helper::nexopos_post( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/woo_order/',
            $details
        );

        // echo "<pre>";
        // print_r($response);
        // die();

        return self::response( $response );
    }

    /**
     * Return a valid response based on the 
     * status of the request
     * @return SyncResponse
     */
    public static function response( $response )
    {
        /**
         * If the system is not connected to NexoPOS. 
         * We might need to send an error message
         */
        if ( $response instanceof Requests_Exception ) {
            return new SyncResponse([
                'status'    =>  'failed',
                'message'   =>  $response->getMessage()
            ]);
        }

        if ( is_array( $response ) ) {
            return new SyncResponse( $response );
        }

        return new SyncResponse( $response->body );
    }

    /**
     * Get Gastro modifiers
     * @return array of modifiers
     */
    public static function getGastroModifiers()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        $response  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/gastro/modifiers/'
        );

        $response   =   self::response( $response );        
        
        return $response->toArray();
    }

    /**
     * Sync customers from Woo to NexoPOS
     * @return SyncResponse
     */
    public static function customersToNexoPOS()
    {
        $customers              =   Helper::get_woo_customers();
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        $response               =   Helper::nexopos_post( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/woo_customers/',
            compact( 'customers' )
        );
        
        return new SyncResponse( $response->body );
    }

    /**
     * Sync customers from NexoPOS to WooCommerce
     * @return SyncResponse
     */
    public static function customersToWooCommerce()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );
        
        $response               =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            rtrim( esc_url( $installationDetails[ 'url' ] ), '/' ) . '/api/nexopos/customers/'
        );

        $result     =   [];

        foreach( json_decode( $response->body, true ) as $customer ) {
            try {
                $user_id = wc_create_new_customer( $customer[ 'EMAIL' ], $customer[ 'username' ] ?? $customer[ 'EMAIL' ], sha1( $customer[ 'EMAIL' ] ) );

                update_user_meta( $user_id, "billing_first_name", $customer[ 'billing_name' ] );
                update_user_meta( $user_id, "billing_last_name", $customer[ 'billing_surname' ] );
                update_user_meta( $user_id, "billing_address_1", $customer[ 'billing_address_1' ] );
                update_user_meta( $user_id, "billing_address_2", $customer[ 'billing_address_2' ] );
                update_user_meta( $user_id, "billing_city", $customer[ 'billing_city' ] );
                update_user_meta( $user_id, "billing_postcode", $customer[ 'billing_pobox' ] );
                update_user_meta( $user_id, "billing_email", $customer[ 'billing_email' ] );

                update_user_meta( $user_id, "shipping_first_name", $customer[ 'shipping_name' ] );
                update_user_meta( $user_id, "shipping_last_name", $customer[ 'shipping_surname' ] );
                update_user_meta( $user_id, "shipping_address_1", $customer[ 'shipping_address_1' ] );
                update_user_meta( $user_id, "shipping_address_2", $customer[ 'shipping_address_2' ] );
                update_user_meta( $user_id, "shipping_city", $customer[ 'shipping_city' ] );
                update_user_meta( $user_id, "shipping_postcode", $customer[ 'shipping_pobox' ] );
                update_user_meta( $user_id, "shipping_email", $customer[ 'shipping_email' ] );
                
                $result[]   =   [
                    'status'    =>  'success',
                    'message'   =>  __( 'The creation of the customer was successful', 'nexostore' )
                ];
                
            } catch( Exception $e ) {
                $result[]   =   [
                    'status'    =>  'failed',
                    'message'   =>  $e->getMessage()
                ];
            }
        }
        
        return new SyncResponse([
            'status'    =>  'success',
            'message'   =>  __( 'The process has been completed.', 'nexostore' ),
            'data'      =>  compact( 'result' )
        ]);
    }
}