<?php
namespace NexoStore\Classes;

use WP_Term_Query;

class Metaboxes
{
    public function __construct()
    {
        $this->view     =   new View;
    }

    /**
     * Register Modifier Metabox
     * @return void
     */
    public function modifiers_meta_box( $post_type )
    {
        global $post;

        if ( $post_type !== 'nexostore-modifiers' ) {
            return false;
        }
        
        $modifiers_groups_raw   =   get_terms([
            'taxonomy'      =>  'nexostore-modifiers-groups',
            'hide_empty'    =>  false
        ]);

        $modifiers_group_options    =   [];
        foreach( $modifiers_groups_raw as $modifier ) {
            $modifiers_group_options[ $modifier->term_id ]  =   $modifier->name;
        }

        $modifiers  =   Sync::getGastroModifiers();

        /**
         * parse modifiers
         */
        if ( isset( $modifiers[ 'status' ] ) && @$modifiers[ 'status' ] === 'failed' ) {
            $view     =   new View;
            $view->data([
                'notice_type'   =>  'notice-error',
                'message'   =>  sprintf( 
                    __( 'We\'ve not been able to connect to Gastro. Please consider <a href="%s">checking if the link still exist</a>. 
                    <br>The server has returned the following message <strong>%s</strong>', 'nexostore' ),
                    admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence' ),
                    esc_html( $modifiers[ 'message' ] )
                )
            ]);

            $modifiers      =   [];
            add_action( 'admin_notices', [ $view, 'notice' ]);
        }

        /**
         * Build option from the modifier
         * array
         */
        $modifier_options   =   [];
        if ( ! empty( $modifiers ) ) {
            foreach( $modifiers as $modifier ) {
                $modifier_options[ $modifier[ 'ID' ] ]      =   $modifier[ 'NAME' ];
            }
        }

        $modifierDetails    =   new View;
        $modifierDetails->path( 'cpt' )->data([
            'fields'  =>  [
                [
                    'type'  =>  'text',
                    'name'  =>  'nexostore_modifier_price',
                    'value' =>  get_post_meta( $post->ID, 'nexostore_modifier_price', true ),
                    'label' =>  __( 'Price', 'nexostore' ),
                    'description'   =>  __( 'Will set the price of the modifier', 'nexostore' )
                ], [
                    'type'  =>  'select',
                    'name'  =>  'nexostore_modifier_id',
                    'options'   =>  $modifier_options,
                    'value' =>  get_post_meta( $post->ID, 'nexostore_modifier_id', true ),
                    'label' =>  __( 'Assigned Modifier', 'nexostore' ),
                    'description'   =>  __( 'Link this modifier with an existing modifier on Gastro.', 'nexostore' )
                ], [
                    'type'  =>  'select',
                    'name'  =>  'nexostore_modifier_selected',
                    'value' =>  get_post_meta( $post->ID, 'nexostore_modifier_selected', true ),
                    'options'   =>  [
                        'yes'   =>  __( 'Yes', 'nexostore' ),
                        'no'    =>  __( 'No', 'nexostore' ),
                    ],
                    'label' =>  __( 'Default', 'nexostore' ),
                    'description'   =>  __( 'Only one modifier can be set as selected by default.', 'nexostore' )
                ], [
                    'type'  =>  'select',
                    'name'  =>  'nexostore_modifier_group_id',
                    'value' =>  get_post_meta( $post->ID, 'nexostore_modifier_group_id', true ),
                    'options'   =>  $modifiers_group_options,
                    'label' =>  __( 'Assign to a group', 'nexostore' ),
                    'description'   =>  __( 'Define to which group the modifier should be assigned to.', 'nexostore' )
                ], [
                    'type'  =>  'editor',
                    'name'  =>  'nexostore_modifier_description',
                    'value' =>  get_post_meta( $post->ID, 'nexostore_modifier_description', true ),
                    'label' =>  __( 'Description', 'nexostore' ),
                    'description'   =>  __( 'Define to which group the modifier should be assigned to.', 'nexostore' )
                ]
            ],
            'view'  =>  new View
        ]);

        /**
         * Add Meta box for modifier fields
         */
        add_meta_box(
            'tagsdiv-nexostore-modifiers-groups',
            __( 'Modifier Details', 'nexostore' ),
            [ $modifierDetails, 'cpt_metabox_fields' ],
            'nexostore-modifiers',
            'normal'
        );
    }
}