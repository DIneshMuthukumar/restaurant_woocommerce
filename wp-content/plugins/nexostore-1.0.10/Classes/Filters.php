<?php
/**
 * Filters Class
 * register all filters hook
 * @since 1.0
 */
namespace NexoStore\Classes;

use NexoStore\Classes\NexoStoreWooSettings;

defined( 'ABSPATH' ) || exit;

class Filters
{
    public function __construct()
    {
        add_filter( 'woocommerce_get_settings_pages', [ $this, 'settings_init' ]);
        add_filter( 'wp_insert_post_data', [ $this, 'filter_post_data' ], 20, 2 );
        add_filter( 'woocommerce_add_cart_item_data', [ $this, 'add_modifiers' ], 20, 2 );
        add_filter( 'woocommerce_get_item_data', [ $this, 'display_modifiers' ], 20, 2 );
    }

    /**
     * Init Settings for NexoStore
     * @param array settings
     * @return array settings
     */
    public function settings_init( $settings )
    {
        $settings[]     =   new NexoStoreWooSettings();
        return $settings;
    }

    /**
     * Filter post data to make sure
     * to cach the sku before it get modified
     * @param array of data
     * @param array of product details
     * @return array of data
     */
    public function filter_post_data( $data, $post )
    {
        if ( is_admin() ) {
            if ( $post[ 'post_type' ] === 'product' && get_current_screen()->action !== 'add' ) {
                
                global $NexoStoreGlobal;
    
                $product    =   wc_get_product( sanitize_text_field( $post[ 'ID' ] ) );
    
                if ( $product ) {
                    $NexoStoreGlobal[ 'sku' ]   =   $product->get_sku();
                }
            }
        }

        return $data;
    }

    /**
     * Add modifier to an item on the cart
     * @return array
     */
    public function add_modifiers( $cart_item, $product_id ) 
    {
        if ( isset( $_POST[ 'nexostoreModifiers' ] ) ) {
            $nexoModifiers  =   $_POST[ 'nexostoreModifiers' ];
            $custom_data    =   [];
            $product        =   wc_get_product( $product_id );
            $total_price    =   $product->get_price();

            foreach( $nexoModifiers as $group ) {
                /**
                 * Filter to only get the selected items
                 */
                $selectedModifiers  =   array_filter( $group[ 'modifiers' ], function( $modifier ) use ( &$total_price ) {
                    // update the price according to the modifier price
                    if ( $modifier[ 'selected' ] === 'yes' ) {
                        $total_price      +=  floatval( $modifier[ 'price' ] );
                        return true;
                    } 
                    return false;
                });

                /**
                 * Fill the modifier details
                 */
                foreach( $selectedModifiers as &$modifier ) {
                    $modifier[ 'group_name' ]          =   $group[ 'group_name' ];
                    $modifier[ 'group_multiselect' ]   =   $group[ 'group_multiselect' ];
                    $modifier[ 'group_forced' ]        =   $group[ 'group_forced' ];
                    $modifier[ 'group_description' ]   =   $group[ 'group_description' ];
                    $modifier[ 'default' ]             =   $modifier[ 'selected' ] === 'yes' ? 1 : 0;
                }

                $group_data     =   array_merge( $group, [
                    'modifiers'     =>  $selectedModifiers
                ]);

                $custom_data[]  =   $group_data;
            }

            $cart_item[ 'nexostore-item-price' ]    =   $total_price;
            $cart_item[ 'nexostore-modifiers' ]     =   $custom_data;
        }

        return $cart_item;
    }

    /**
     * Display modifiers on the cart
     * @return array
     */
    public function display_modifiers( $data, $cart_item )
    {
        if ( isset( $cart_item[ 'nexostore-modifiers' ] ) ) {
            foreach( $cart_item[ 'nexostore-modifiers' ] as $group ) {
                foreach( $group[ 'modifiers' ] as $modifier ) {
                    $data[]     =   [
                        'name'      =>  $group[ 'group_name' ],
                        'display'   =>  $modifier[ 'name' ] . ' &mdash; ' . wc_price( $modifier[ 'price' ] )
                    ];
                }
            }
        }

        return $data;
    }
}