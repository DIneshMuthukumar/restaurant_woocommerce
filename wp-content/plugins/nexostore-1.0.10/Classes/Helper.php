<?php
/**
 * Helper Class
 * register helper functions
 * @since 1.0
 */

namespace NexoStore\Classes;

use Requests;
use WC_Product;

defined( 'ABSPATH' ) || exit;

class Helper {

    /**
     * retur full URL for a defined url
     * @param string url
     */
    public static function registerCSS( $url )
    {
        $file   =   basename( $url );
        wp_register_style( 'nexostore-' . $file, plugins_url( 'assets/css/' . $url . '.css', NEXOSTORE_FILEPATH ), false, '1.0.0' );
        wp_enqueue_style( 'nexostore-' . $file );
    }

    /**
     * register js script
     * @return void
     */
    public static function registerJS( $url, $deps = [], $footer = false )
    {
        $file   =   basename( $url );
        wp_register_script( 'nexopos-js-' . $file, plugins_url( 'assets/js/' . $url . '.js', NEXOSTORE_FILEPATH ), $deps, $footer );
        wp_enqueue_script( 'nexopos-js-' . $file );
    }

    /**
     * Proceed to a Request Query
     * @return request response object
     */
    public static function get( $url, $option = null )
    {
        $headers    =   [ 
            'X-API-TOKEN'       =>  get_option( 'nexostore_access_token' ), 
            'X-Requested-With'  =>  'XMLHttpRequest',
            'X-Client-App'      =>  'NS-BETA'
        ];

        try {
            $request    =   Requests::get( $url, $headers );
        } catch( \Exception $e ) {
            return $e;
        }

        return $request;
    }

    /**
     * Proceed to a POST Request Query
     * @return request response object
     */
    public static function post( $url, $data = [])
    {
        $headers    =   [ 
            'X-API-TOKEN'       =>  get_option( 'nexostore_access_token' ), 
            'X-Requested-With'  =>  'XMLHttpRequest',
            'X-Client-App'      =>  'NS-BETA'
        ];

        try {
            $request    =   Requests::post( $url, $headers, $data );
        } catch( \Exception $e ) {
            return $e;
        }

        return $request;
    }

    /**
     * Server URl
     * @return string
     */
    public static function serverUrl( $url )
    {
        return NEXOSTORE_STORE_URL . $url;
    }

    /**
     * Set Option
     */
    public static function set_option( $key, $value, $default = '' ) {
        if ( FALSE === get_option( $key ) && FALSE === update_option( $key, FALSE ) ) {
            add_option( $key, $value );
        }
    }

    /**
     * get method for nexopos installation
     * @param string
     * @return HTTPResponse
     */
    public static function nexopos_get( $rest_key, $url )
    {
        $headers    =   [ 'X-API-KEY' => $rest_key, 'X-Requested-With' => 'XMLHttpRequest' ];
        
        try {
            $request    =   Requests::get( $url, $headers );
        } catch( \Exception $e ) {
            return $e;
        }

        return $request;
    }

    /**
     * get method for nexopos installation
     * @param string
     * @return HTTPResponse
     */
    public static function nexopos_post( $rest_key, $url, $data )
    {
        $headers    =   [ 'X-API-KEY' => $rest_key, 'X-Requested-With' => 'XMLHttpRequest' ];

        // echo "<pre>";
        // print_r($data);
        // die();
        
        try {
            $request    =   Requests::post( $url, $headers, $data );
        } catch( \Exception $e ) {
            return $e;
        }

        return $request;
    }

    /**
     * get WooCommerce product
     * using a SKU. This perform a SQL query directly
     * @param string
     * @return WC_Product|null
     */
    public static function get_product_by_sku( $sku ) {

        global $wpdb;
    
        $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );
    
        if ( $product_id ) return new WC_Product( $product_id );
    
        return null;
    }
    
    /**
     * Delete Method for NexoPOS Installation
     * @param string rest key
     * @param string url
     * @return HTTPResponse
     */
    public static function nexopos_delete( $rest_key, $url )
    {
        $headers    =   [ 'X-API-KEY' => $rest_key, 'X-Requested-With' => 'XMLHttpRequest' ];
        
        try {
            $request    =   Requests::delete( $url, $headers );
        } catch( \Exception $e ) {
            return $e;
        }

        return $request;
    }

    public static function get_link( $link )
    {
        $id    			        =   'nexostore';
        $client_key 		    =	'3VWg7wtEwjqzVHKvTIOuOaIZzzSXhc1gakngjXfo';
        $client_secret 	        =	'yC0qCwbuOdz5JeJ37tGl8mX2cdRH8t60ovo0efje';
        $store     		        =   NEXOSTORE_STORE_URL;

        $callback_url 	        =	admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence' );
        $redirect_url 	        =	$store . 'oauth?scopes=nexopos.envato-licences,nexopos.sync&client_secret=' . $client_secret . '&client_key=' . $client_key . '&callback_url=' . urlencode( $callback_url );
        $logout_url 	        =	admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&platform-logout=true' );

        switch( $link ) {
            case 'nexopos_platform_auth' : return $redirect_url;
            case 'nexopos_platform_logout'	: return  $logout_url;
            default: return '#';
        }
    }

    /**
     * get WooCommerce customers
     * @return array 
     */
    public static function get_woo_customers()
    {
        $rawCustomers   =   get_users( 'orderby=nicename&role=customer' );
        $customers      =   [];

        foreach( $rawCustomers as $rawCustomer ) {
            $customer       =   [
                'id'        =>  $rawCustomer->ID,
                'login'     =>  $rawCustomer->data->user_login,
                'email'     =>  $rawCustomer->user_email
            ];

            foreach([ 'shipping', 'billing' ] as $method ) {
                foreach([
                    'first_name',
                    'last_name',
                    'address_1',
                    'company',
                    'city',
                    'state',
                    'postcode',
                    'phone', 
                    'email', 
                    'method',
                    'country'
                ] as $field ) {
                    $key    =   $method . '_' . $field;
                    if ( $value = get_user_meta( $rawCustomer->ID, $key, true ) ) {
                        $customer[ $key ]   =   $value;
                    }
                }
            }

            $customers[]    =   $customer;
        }

        return $customers;
    }

    /**
     * 
     */
    public static function slugify( $string, $param = '-' ) 
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', $param, $string), '-'));
    }
}