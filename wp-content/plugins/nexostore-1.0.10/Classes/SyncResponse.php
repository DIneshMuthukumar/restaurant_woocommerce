<?php
namespace NexoStore\Classes;

class SyncResponse
{
    public function __construct( $response )
    {
        $this->responseArray        =   is_array( $response ) ? $response : json_decode( $response, true );
        $this->responseJson         =   is_string( $response ) ? $response : json_encode( $response );
        $this->response             =   $response;
    }

    /**
     * To Array
     * get a response as an array
     * @return array
     */
    public function toArray()
    {
        return $this->responseArray;
    }

    /**
     * to Json
     * get the provided response as a json
     * @return string
     */
    public function toJson()
    {
        return $this->responseJson;
    }

    /**
     * to Raw
     * @return string
     */
    public function toRaw()
    {
        return $this->response;
    }
}