<?php
namespace NexoStore\Classes;

use WP_Term;
use WP_Query;

class Product
{
    /**
     * Get product groups and modfiies
     * @param int product id
     * @return array
     */
    public static function get_product_modifiers( $product_id )
    {
        $product            =   wc_get_product( sanitize_text_field( $product_id ) );
        $modifiers_group    =   get_post_meta( $product->get_id(), 'nexostore_selected_modifier_group', true );
        
        if ( ! empty( $modifiers_group ) ) {
            $modifiersData      =   [];
            foreach( $modifiers_group as $group_id ) {

                /**
                 * Get group details
                 */
                $group      =   get_term( $group_id, 'nexostore-modifiers-groups' );

                if ( $group instanceof WP_Term ) {
                    /**
                     * Build group data
                     * that will be used to fill the modifiers
                     */
                    $groupData  =   [
                        'group_id'              =>  $group_id,
                        'group_name'            =>  $group->name,
                        'group_description'     =>  $group->description,
                        'group_forced'          =>  get_term_meta( $group_id, 'modifier_group_forced', true ),
                        'group_multiselect'     =>  get_term_meta( $group_id, 'modifier_group_multiselect', true ),
                        'modifiers'             =>  []
                    ];

                    $modifiers  =   new WP_Query([
                        'post_type'     =>  'nexostore-modifiers',
                        'meta_query'     =>  [
                            [
                                'key'       =>  'nexostore_modifier_group_id',
                                'value'   => $group_id
                            ]
                        ]
                    ]);

                    $modifiers_posts    =   $modifiers->get_posts();
                    if ( ! empty( $modifiers_posts ) ) {
                        foreach( $modifiers_posts as $modifier ) {
                            $groupData[ 'modifiers' ][]     =   [
                                'name'          =>  $modifier->post_title,
                                'selected'      =>  get_post_meta( $modifier->ID, 'nexostore_modifier_selected', true ),
                                'group_id'      =>  get_post_meta( $modifier->ID, 'nexostore_modifier_group_id', true ),
                                'description'   =>  get_post_meta( $modifier->ID, 'nexostore_modifier_description', true ),
                                'price'         =>  get_post_meta( $modifier->ID, 'nexostore_modifier_price', true ),
                                'gastro_id'     =>  get_post_meta( $modifier->ID, 'nexostore_modifier_id', true ),
                                'thumbnail'     =>  get_the_post_thumbnail_url( $modifier->ID ),
                            ];
                        }
                    }

                    $modifiersData[]    =   $groupData;
                }
            }

            return $modifiersData;
        }
        return false;
    }
}