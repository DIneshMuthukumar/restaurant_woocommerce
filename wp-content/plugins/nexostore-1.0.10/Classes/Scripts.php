<?php
namespace NexoStore\Classes;

class Scripts
{
    /**
     * Add public file for the WooCommerce 
     * Store
     * @return void
     */
    public static function frontend_scripts()
    {
        Helper::registerJS( '../bower_components/axios/dist/axios.min' );
        Helper::registerJS( '../bower_components/vue/dist/vue.min' );
        Helper::registerJS( 'sweet-alert.min' );
        Helper::registerCSS( 'modifier-style' );
        Helper::registerCSS( 'isolated.bootstrap.min' );
        
        /**
         * Bootstrap 4 Date Time picker
         */
        wp_enqueue_script( 'nexostore-moment', NEXOSTORE_PLUGIN_URL . 'assets/node_modules/moment/moment.js' );
        wp_enqueue_script( 'nexostore-moment-timezone', NEXOSTORE_PLUGIN_URL . 'assets/node_modules/moment-timezone/moment-timezone.js' );
        Helper::registerJS( '../node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min', [ 'jquery', 'nexostore-moment', 'nexostore-moment-timezone' ], true );
        Helper::registerCSS( '../node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min' );

        $textDomain     =   [
            'pleaseWait'            =>  __( 'Please wait', 'nexostore' ),
            'modifierRequiredText'  =>  __( 'You need to select at least one modifier before proceeding.', 'nexostore' ),
            'modifierRequired'      =>  __( 'Please Select A modifier.', 'nexostore' ),
            'confirmText'           =>  __( 'Ok', 'nexostore' ),
            'loading'               =>  __( 'Loading...', 'nexostore' ),
            'chooseModifier'        =>  __( 'Add to the Cart', 'nexostore' ),
        ];

        // wp_enqueue_script( 'nexostore-datetime', NEXOSTORE_PLUGIN_URL . 'assets/js/vue-datetime-1.x/.js' );
        wp_enqueue_script( 'nexostore-watch', NEXOSTORE_PLUGIN_URL . 'assets/js/nexostore-watch.js', [ 'jquery' ], true );
        wp_localize_script( 'nexostore-watch', 'NexoStoreWatchData', [
            'server_time'           =>   current_time( 'mysql' )
        ]);     

        wp_enqueue_script( 'nexostore-button-watcher', NEXOSTORE_PLUGIN_URL . 'assets/js/nexostore-add-to-card-watcher.js', [ 'jquery' ], true );
        wp_localize_script( 'nexostore-button-watcher', 'NexoStoreAddToCartData', [
            'currency_position'     =>   get_option( 'woocommerce_currency_pos' ),
            'currency_symbol'       =>   get_woocommerce_currency_symbol( get_option( 'woocommerce_currency' ) ),
            'currency'              =>  get_option( 'woocommerce_currency' ),
            'textDomain'            =>  $textDomain,
            'ajaxurl'               =>  admin_url( 'admin-ajax.php' )
        ]);        

        if ( is_product() ) {
            wp_enqueue_script( 'nexostore-single-watcher', NEXOSTORE_PLUGIN_URL . 'assets/js/nexostore-single-watcher.js', [ 'jquery' ], true );
            wp_localize_script( 'nexostore-single-watcher', 'NexoStoreSingleProductData', [
                'currency_position'     =>   get_option( 'woocommerce_currency_pos' ),
                'currency_symbol'       =>   get_woocommerce_currency_symbol( get_option( 'woocommerce_currency' ) ),
                'currency'              =>  get_option( 'woocommerce_currency' ),
                'textDomain'            =>  $textDomain,
                'ajaxurl'               =>  admin_url( 'admin-ajax.php' )
            ]);
        } else if ( is_cart() ) { // we should choose the right page to display it
            wp_enqueue_script( 'nexostore-checkout-watcher', NEXOSTORE_PLUGIN_URL . 'assets/js/nexostore-checkout-watcher.js', [ 'jquery' ], true );
            wp_localize_script( 'nexostore-checkout-watcher', 'NexoStoreCheckoutVueData', [
                'currency_position'             =>   get_option( 'woocommerce_currency_pos' ),
                'currency_symbol'               =>   get_woocommerce_currency_symbol( get_option( 'woocommerce_currency' ) ),
                'currency'                      =>  get_option( 'woocommerce_currency' ),
                'dinein_shipping_method'        =>  get_option( 'nexostore_dinein_method' ), // need to pull from options
                'takeaway_shipping_method'        =>  get_option( 'nexostore_takeaway_method' ), // need to pull from options
                'delivery_shipping_method'      =>  '', // need to pull from options
                'default_restaurant_zone'       =>  get_option( 'nexostore_shipping_zone' ), // the default store location is used for dinein and takeaway
                'textDomain'            =>  [
                ],
                'orderTypes'            =>  [
                    // [
                    //     'namespace'     =>  'dinein',
                    //     'text'          =>  __( 'Dine in', 'nexostore' ),
                    //     'image'         =>  NEXOSTORE_PLUGIN_URL . '/assets/images/dinein-full.png',
                    //     'description'   =>  ! empty( get_option( 'nexostore_dinein_text' ) ) ? get_option( 'nexostore_dinein_text' ) : __( 'Book a table at the restaurant. Only available tables will be listed.', 'nexostore' )
                    // ],
                    // [
                    //     'namespace'     =>  'takeaway',
                    //     'text'          =>  __( 'Pick Up', 'nexostore' ),
                    //     'image'         =>  NEXOSTORE_PLUGIN_URL . '/assets/images/takeaway-full.png',
                    //     'description'   =>  ! empty( get_option( 'nexostore_takeaway_text' ) ) ? get_option( 'nexostore_takeaway_text' ) : __( 'If you would like to pick up the meal once ready at the restaurant.', 'nexostore' )
                    // ],
                    [
                        'namespace'     =>  'delivery',
                        'text'          =>  __( 'Delivery', 'nexostore' ),
                        'image'         =>  NEXOSTORE_PLUGIN_URL . '/assets/images/delivery-full.png',
                        'description'   =>  ! empty( get_option( 'nexostore_delivery_text' ) ) ? get_option( 'nexostore_delivery_text' ) : __( 'Receive the order to your location using your delivery informations.', 'nexostore' )
                    ],
                ],
                'ajaxurl'               =>  admin_url( 'admin-ajax.php' )
            ]);
        }
    }

    /**
     * Add Admin Styles
     */    
    public static function admin_styles( $hook )
    {
        if ( $hook === 'settings_page_nexostore-settings' ) {
            Helper::registerCSS( 'grid.min' );
        }
        
        Helper::registerCSS( 'admin' );
        Helper::registerJS( '../bower_components/axios/dist/axios.min' );
        Helper::registerJS( '../bower_components/vue/dist/vue.min' );
        Helper::registerJS( 'nexostore-helper' );
        Helper::registerJS( 'queable-promises' );
        Helper::registerJS( 'ping-nexoplatform' );
        Helper::registerJS( 'sync.vue' );
        Helper::registerJS( 'nexostore-core' );
        Helper::registerJS( 'sweet-alert.min' );

        wp_localize_script( 'nexopos-js-nexostore-core', 'NexoStoreData', [
            'textDomain'   =>  [
                'gastro_missing'            =>  sprintf( __( 'Unable to retreive the module Gastro from that installation. Please check if NexoPOS is linked to your <a href="%s" target="_blank">NexoPOS Platform account</a>, or if the module is enabled on NexoPOS.', 'nexostore' ), Helper::serverUrl( 'dashboard/profile/envato-licences' ) ),
                'gastro_version_issue'      =>  __( 'The version of Gastro available on that system is not supported. Please install a more recent verison.', 'nexostore' ),
                'nexopos_version_issue'     =>  __( 'The version of NexoPOS available on the system is not supported. Please install a more recent version.', 'nexostore' ),
                'nexopos_success'           =>  __( 'NexoPOS is now successfully linked to WooCommerce.', 'nexostore' ),
                'skip'                      =>  __( 'Skip the notice.', 'nexostore' ),
                'ping_error'                =>  __( 'An error has occured while sending a ping request to the website.', 'nexostore' ),
                'sync_type_missing'         =>  __( 'Please select a Sync Option before proceeding.', 'nexostore' ),
                'error_occured'             =>  __( 'An error occured', 'nexostore' ),
                'access_denied'             =>  __( 'Unable to access to the system. It seems like WooCommerce has lost the access. Please consider refreshing the connection', 'nexostore' ),
                'multistore_enabled_error'  =>  __( 'It seems like a multistore mode is enabeld on your system. Note that Nexo Store only works with if the multistore mode is disabled.', 'nexostore' ),
                'confirm_sync'              =>  __( 'Would you like to proceed ?', 'nexostore' ),
                'confirm_text_sync'         =>  __( 'If you sync from WooCommerce, your NexoStore products will be replaced. If you sync from NexoPOS your WooCommerce products will be replaced.', 'nexostore' ),
                'confirm_proceed_sync'      =>  __( 'Yes, Proceed', 'nexostore' ),
                'title_check_options'       =>  __( 'Check the installation options...', 'nexostore' ),
                'title_categories'          =>  __( 'Sync the categories...', 'nexostore' ),
                'title_items'               =>  __( 'Sync the items...', 'nexostore' ),
                'syncSuccessTitle'          =>  __( 'The sync was successful', 'nexostore' ),
                'sync_error_occured'        =>  __( 'An unexpected error occured during the syncronization...', 'nexostore' ),
                'syncSuccessMessage'        =>  __( 'The operation was successful, enjoy using Nexo Store !', 'nexostore' ),
            ]
        ]);        
    }
}