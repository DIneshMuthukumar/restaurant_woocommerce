<?php
/**
 * Manage all Ajax actions
 */
namespace NexoStore\Classes;

use NexoStore\Classes\Helper;
use NexoStore\Classes\Sync;
use WP_Query;
use WP_Term;
use Requests_Exception;

class Ajax
{
    public function construct()
    {
        $this->view     =   new View;
    }
    
    /**
     * Return a JSON response
     * @return json
     */
    private function response( $response )
    {
        if ( is_array( $response ) ) {
            echo json_encode( $response );
        } else {
            echo $response;
        }
        wp_die();
    }

    /**
     * NexoStore Save installation
     * @return void
     */
    public function nexostore_save_installation()
    {
        global $wpdb;
        
        /** 
         * delete option to ensure that
         * the new option can be saved
         */
        delete_option( 'nexostore_installation_details' );
        Helper::set_option( 'nexostore_installation_details', $_POST[ 'installation_data' ] );

        echo json_encode([
            'status'    =>  'success',
            'message'   =>  __( 'The installation has been successfully saved' )
        ]);

        wp_die();
    }

    /**
     * Get Details from the installation
     * @return json
     */
    public function nexostore_sync_categories()
    {

        // echo "<pre>";
        // print_r($_POST);
        // die();
        if ( $_POST[ 'sync_option' ] === 'sync_to_nexopos' ) {
            echo Sync::categoryToNexoPOS()->toJson();
        } else if ( $_POST[ 'sync_option' ] === 'sync_to_woocommerce' ) {
            echo Sync::categoryToWooCommerce()->toJson();
        }

        wp_die();
    }

    /**
     * get options from the installation
     * @return json
     */
    public function nexo_check_options()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        $options  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/options'
        );

        echo $options->body;

        wp_die(); 
    }

        /**
     * Ping website
     * @return json
     */
    public function nexo_platform_ping_website()
    {
        $result                     =   Helper::get( Helper::serverUrl( 'api/nexopos/envato-licences/16195010' ) );  
        $licences                   =   json_decode( $result->body );

        $licence                    =   array_values( array_filter( $licences, function( $licence ) {
            return intval( $licence->id ) === intval( sanitize_text_field( $_POST[ 'installation_id' ]) );
        }) );

        if ( count( $licence ) > 0 && $licence[0]->rest_key !== null ) {
            $request    =   Helper::nexopos_get( 
                $licence[0]->rest_key, 
                rtrim( $licence[0]->url, '/' ) . '/api/nexopos/system-details', 
                [ 
                    'X-API-KEY'         => $licence[0]->rest_key, 
                    'X-Requested-With'  => 'XMLHttpRequest' 
                ], [
                    'installation_id'   =>  sanitize_text_field( $_POST[ 'installation_id' ])
                ]
            );

            echo Sync::response( $request )->toJson();

        } else {
            echo json_encode([
                'status'    =>  'failed',
                'message'   =>  __( 'Unable to ping Gastro' )
            ]);
        }

        wp_die();
    }

    /**
     * Sync system details
     * @return json
     */
    public function nexostore_sync_details( $silent = false )
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        $options  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/options'
        );

        /**
         * if the silence is required
         * no need to output something
         */
        if ( $silent === false ) {
            echo $options->body;
            wp_die(); 
        }
    }

    /**
     * Sync items down from WooCommerce to NexoPOS
     * @return json
     */
    public function nexostore_sync_items()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        $options  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/options'
        );

        if ( $_POST[ 'sync_option' ] === 'sync_to_nexopos' ) {
            echo Sync::itemsToNexoPOS()->toJson();
        } else if ( $_POST[ 'sync_option' ] === 'sync_to_woocommerce' ) {
            echo Sync::itemsToWooCommerce()->toJson();
        }

        /**
         * if the silence is required
         * no need to output something
         */
        if ( $silent === false ) {
            echo $options->body;
        }
        wp_die(); 
    }

    /**
     * Check Modifier for a specific item
     * @return void
     */
    public function nexo_check_modifiers()
    {        
        if ( ( $response  = Product::get_product_modifiers( $_POST[ 'product_id' ] ) ) !== false ) {
            echo json_encode( $response );
            wp_die();            
        }

        echo json_encode([
            'status'    =>  'failed',
            'message'   =>  __( 'Unable to find modifier group attached to the item.', 'nexostore' )
        ]);

        wp_die();
    }

    /**
     * Get available tables from Gastro
     * @return json
     */
    public function nexostore_available_tables() 
    {

        // echo "<pre>";
        // print_r(__LINE__);
        // die();

      
        //nexostore_installation_details
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        // echo "<pre>";
        // print_r($installationDetails);
        // die();
        /**
         * the website is not correctly configured
         */
      
        if ( $installationDetails[ 'url' ] === null ) {
            echo json_encode([
                'status'    =>  'failed',
                'message'   =>  __( 'It seems like the NexoPOS is not correctly synced. Check the "Licence & Update Settings" and try again!', 'nexostore' )
            ]);

            wp_die();
        }

        $options  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/gastro/tables'
        );

        /**
         * if an exception is returned 
         * by the server
         */
        if ( $options instanceof Requests_Exception ) {
            echo json_encode([
                'status'    =>   'failed',
                'message'   =>  $options->getMessage()
            ]);
            wp_die();
        }

        echo $options->body;

        wp_die(); 
    }

    /**
     * Sync customers from WooCommerce to NexoPOS
     * @return json
     */
    public function nexostore_sync_customers()
    {
        $installationDetails    =   json_decode( stripslashes( get_option( 'nexostore_installation_details' ) ), true );

        $options  =   Helper::nexopos_get( 
            $installationDetails[ 'rest_key' ],
            esc_url( rtrim( $installationDetails[ 'url' ], '/' ) ) . '/api/nexopos/options'
        );

        if ( $_POST[ 'sync_option' ] === 'sync_to_nexopos' ) {
            echo Sync::customersToNexoPOS()->toJson();
        } else if ( $_POST[ 'sync_option' ] === 'sync_to_woocommerce' ) {
            echo Sync::customersToWooCommerce()->toJson();
        }

        wp_die();
    }

    /**
     * Save Meta to the card
     * These information will be used later
     * @return json
     */
    public function nexostore_save_order_metas()
    {
        $pureData   =   [];

        foreach([
            'nexostore_datetime' ,
            'nexostore_table_seats' ,
            'nexostore_table_id' ,
            'nexostore_order_type' ,
        ] as $field ) {
            $pureData[ $field ]     =   sanitize_text_field( $_POST[ $field ] );
            WC()->session->set( $field, sanitize_text_field( $_POST[ $field ] ) );
        }

        /**
         * The meta has been successfully set
         * let's notify the listener
         */
        echo json_encode([
            'status'    =>  'success',
            'message'   =>  __( 'The meta has been successfully set', 'nexostore' )
        ]);

        wp_die();
    }
}