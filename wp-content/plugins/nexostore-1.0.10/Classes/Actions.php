<?php
/**
 * Action Class
 * register all action hook
 * @since 1.0
 */
namespace NexoStore\Classes;

use NexoStore\Classes\View;
use NexoStore\Classes\Helper;
use NexoStore\Classes\Ajax;
use NexoStore\Classes\Sync;
use NexoStore\Classes\CPT;
use NexoStore\Classes\Fields;
use NexoStore\Classes\Metaboxes;


use WC_Customer;
use WP_Query;
use WC_Cart;

defined( 'ABSPATH' ) || exit;

class Actions 
{
    private $view;
    private $WooCommerceActive;
    public function __construct()
    {     
        $this->view         =   new View;
        $this->ajax         =   new Ajax;
        $this->cpt          =   new CPT;
        $this->fields       =   new Fields;
        $this->metaBoxes    =   new MetaBoxes;
        
        /**
         * register Custom Post Types
         */
        add_action( 'init', [ $this->cpt, 'register_modifiers' ]);
        add_action( 'save_post', [ $this->cpt, 'save_modifier' ], 10, 3 );
        add_action( 'save_post', [ $this->fields, 'save_product' ], 10, 3 );

        /**
         * register custom fields for post type and taxonomies
         */
        add_action( 'nexostore-modifiers-groups_add_form_fields', [ $this->fields, 'register_create_taxonomy_fields' ]);
        add_action( 'nexostore-modifiers-groups_edit_form_fields', [ $this->fields, 'register_edit_taxonomy_fields' ]);
        add_action( 'created_nexostore-modifiers-groups', [ $this->cpt, 'save_modifier_group' ]);
        add_action( 'edited_nexostore-modifiers-groups', [ $this->cpt, 'save_modifier_group' ]);
        
        /**
         * WooCommerce hooks
         */
        add_action( 'woocommerce_admin_field_button', [ $this, 'auth_field' ]);
        add_action( 'woocommerce_admin_field_paragraph', [ $this, 'paragraph' ]);
        add_action( 'woocommerce_admin_field_site_select', [ $this, 'site_select' ]);
        add_action( 'woocommerce_admin_field_link', [ $this->fields, 'link' ]);
        add_action( 'woocommerce_admin_field_hidden_field', [ $this, 'hidden_field' ]);
        add_action( 'woocommerce_checkout_update_order_meta', [ $this, 'watch_orders' ]);
        add_action( 'woocommerce_settings_saved', [ $this, 'woo_save_settings' ]);
        add_action( 'woocommerce_product_options_general_product_data', [ $this->fields, 'woo_add_field' ]);
        add_action( 'woocommerce_before_calculate_totals', [ $this, 'calculate_totals' ], 20, 1 );
        add_action( 'woocommerce_before_add_to_cart_button', [ $this, 'show_modifier_selector' ]);
        add_action( 'woocommerce_after_checkout_billing_form', [ $this->fields, 'add_checkout_fields' ]);
        add_action( 'woocommerce_before_cart', [ $this, 'show_order_type' ]);
        add_action( 'woocommerce_checkout_create_order', [ $this, 'save_order_meta' ], 20, 2 );
        add_action( 'woocommerce_checkout_create_order_line_item', [ $this, 'add_modifier_data_to_order' ], 20, 4 );
        add_action( 'woocommerce_update_cart_action_cart_updated ', [ $this, 'update_cart_action' ]);
        // add_action( 'woocommerce_shipping_init', [ $this, 'register_shipping_class' ]);
        
        /**
         * Ajax Hooks
         */
        add_action( 'wp_ajax_' . 'nexostore_ping_platform_gastro', [ $this->ajax, 'nexo_platform_ping_website' ]);
        add_action( 'wp_ajax_' . 'nexostore_save_installation', [ $this->ajax, 'nexostore_save_installation' ]);
        add_action( 'wp_ajax_' . 'nexostore_sync_categories', [ $this->ajax, 'nexostore_sync_categories' ]);
        add_action( 'wp_ajax_' . 'nexostore_sync_customers', [ $this->ajax, 'nexostore_sync_customers' ]);
        add_action( 'wp_ajax_' . 'nexostore_sync_details', [ $this->ajax, 'nexostore_sync_details' ]);
        add_action( 'wp_ajax_' . 'nexostore_sync_items', [ $this->ajax, 'nexostore_sync_items' ]);
        add_action( 'wp_ajax_' . 'nexo_check_options', [ $this->ajax, 'nexo_check_options' ]);
        add_action( 'wp_ajax_' . 'nexostore_check_modifiers', [ $this->ajax, 'nexo_check_modifiers' ]);
        add_action( 'wp_ajax_' . 'nexostore_available_tables', [ $this->ajax, 'nexostore_available_tables' ]);
        add_action( 'wp_ajax_' . 'nexostore_save_order_metas', [ $this->ajax, 'nexostore_save_order_metas' ]);
        add_action( 'wp_ajax_' . 'nopriv_' . 'nexostore_check_modifiers', [ $this->ajax, 'nexo_check_modifiers' ]);
        add_action( 'wp_ajax_' . 'nopriv_' . 'nexostore_available_tables', [ $this->ajax, 'nexostore_available_tables' ]);
        add_action( 'wp_ajax_' . 'nopriv_' . 'nexostore_save_order_metas', [ $this->ajax, 'nexostore_save_order_metas' ]);
        
        /**
         * Basic WordPress hooks
         */
        add_action( 'admin_notices', [ $this, 'notices' ]);
        add_action( 'wp_trash_post', [ $this, 'wp_trash_post' ]);
        add_action( 'updated_option', [ Sync::class, 'checkOptionUpdate' ], 10, 3 );
        add_action( 'save_post', [ Sync::class, 'createProduct' ], 10, 3 );
        add_action( 'admin_enqueue_scripts', [ Scripts::class, 'admin_styles']);
        add_action( 'wp_enqueue_scripts', [ Scripts::class, 'frontend_scripts' ]);
        
        /**
         * Register custom meta boxes
         */
        add_action( 'add_meta_boxes', [ $this->metaBoxes, 'modifiers_meta_box' ]);
    }

    /**
     * Save order meta
     * @return void
     */
    public function save_order_meta( $order, $data )
    {



        $order->update_meta_data( 'nexostore_order_type', sanitize_text_field( $_POST[ 'nexostore_order_type' ] ) );
        $order->update_meta_data( 'nexostore_table_id', sanitize_text_field( $_POST[ 'nexostore_table_id' ] ) );
        $order->update_meta_data( 'nexostore_table_seats', sanitize_text_field( $_POST[ 'nexostore_table_seats' ] ) );
        $order->update_meta_data( 'nexostore_datetime', sanitize_text_field( $_POST[ 'nexostore_datetime' ] ) );


        // echo "<pre>";
        // print_r($order);
        // die();
    }

    /**
     * Show order type
     * @return void
     */
    public function show_order_type()
    {
        $this->view
            ->path( 'woocommerce' )
            ->checkout_order_type();
    }

    /**
     * Watch all orders and submit that to 
     * NexoPOS
     * @param void
     * @return mixed
     */
    public function watch_orders( $order_id )
    {

        $transient  =   get_transient( 'nexostore_watched_' . $order_id );

      //  if ( $transient === false ) {

            /**
             * We also need to make sure an order has not yet been handled by NexoPOS
             */
            if ( ! $order_id ) {
                return false;
            }
    
            $order  =   wc_get_order( $order_id );


            // echo "<pre>";
            // print_r($order);
            // die();

            /**
             * We might need to check if we can proceed to 
             * an unpaid order as well. Before sending to NexoPOS, we should check if the
             * order is paid or not
             */
            if ( ( $order->is_paid() || ( get_option( 'nexostore_sync_allow_unpaid_order' ) && ! $order->is_paid() ) ) ) {
    
                $details    =   $order->get_data();
                $customer   =   new WC_Customer( $order->get_customer_id() );

                $products       =   array_map( function( $item ) {
                    $product    =   $item->get_product();
                    return array_merge(
                        $item->get_data(),
                        [
                            'sku'           =>  $product->get_sku()
                        ]
                    );
                }, $details[ 'line_items' ] );

                
                /**
                 * populate the modifiers save using Transient API
                 */
                foreach( $products as &$product ) {
                    // turn object into an array
                    $_product   =   json_decode( json_encode( $product ), true );

                    $modifiers  =   array_values( array_filter( $_product[ 'meta_data' ], function( $meta ) {
                        return $meta[ 'key' ]   === '$modifiers';
                    }) );

                    /**
                     * Remove meta from object
                     */
                    wc_delete_order_item_meta( $product[ 'id' ], '$modifiers' );

                    $product[ 'nexostore-modifiers' ]   =   json_decode( @$modifiers[0][ 'value' ] );
                }

                $order  =   [
                    'order'     =>  $details,
                    'products'  =>  $products,
                    'customer'  =>  $customer->get_data()
                ];

            


                $json   =   $this->view->json( Sync::orderToNexoPOS([
                    'data'      =>  json_encode( $order )
                ])->toArray() );

                /**
                 * Only for debugging purpose
                 */
                if ( NEXOSTORE_DEBUG === true ) {
                    file_put_contents( NEXOSTORE_ROOT_PATH . '/order.json', json_encode( $order ) );
                    file_put_contents( NEXOSTORE_ROOT_PATH . '/response.json', $json );
                }

                return $json;
            }
        //}
    }

    /**
     * Update cart action.
     * This help us to update the cart data
     * based on the restaurant selection
     * @return void
     */
    public function update_cart_action()
    {
        /**
         * Let's save the order meta
         */
        var_dump( WC()->order_factory->get_order() );die;

        foreach([ 
            'nexostore_order_type',
            'nexostore_table_id',
            'nexostore_table_seats',
            'nexostore_datetime'
        ] as $field ) {
            WC()->cart->set_value( $order_id, $field, sanitize_text_field( $_POST[ $field ] ) );
        }
    }

    /**
     * Generate an auth button for Authentication
     * with nexopos
     * @param string
     * @return void
     */
    public function auth_field( $value ) 
    {
        include( dirname( __FILE__ ) . '/../views/custom/button.php' );
    }

    /**
     * Generate a text field for WooCommerce
     * with nexopos
     * @param string
     * @return void
     */
    public function paragraph( $value ) 
    {
        include( dirname( __FILE__ ) . '/../views/custom/paragraph.php' );
    }
    
    /**
     * Generate a text field for WooCommerce
     * with nexopos
     * @param string
     * @return void
     */
    public function site_select( $value ) 
    {
        include( dirname( __FILE__ ) . '/../views/custom/site_select.php' );
    }

    /**
     * Generate a hidden field for WooCommerce
     * with nexopos
     * @param string
     * @return void
     */
    public function hidden_field( $value ) 
    {
        include( dirname( __FILE__ ) . '/../views/custom/hidden_field.php' );
    }

    /**
     * Register notifications
     * @return void
     */
    public function notices()
    {
        foreach( array_keys( $_GET ) as $key ) {
            if ( in_array( $key, [ 'platform-connected', 'platform-logged-out', 'not-authenticated' ]) ) {
                include( dirname( __FILE__ ) . '/../views/notices/' . $key . '.php' );
            }
        }
    }

    /**
     * Delete POST
     * proceed to a review of the deleted CPT and 
     * send a delete request to NexoPOS as well.
     * @param int 
     * @return void
     */
    public function wp_trash_post( $id )
    {
        $product =  wc_get_product( $id );
        
        if ( $product !== false && $product instanceof WC_Product_Simple ) {
            if ( $product instanceof WC_Product_Simple ) {
                Sync::deleteProduct( $product );
            } 
        }
    }

    /**
     * Woo Save Settings
     * catch even when settings are saved. We would like
     * to force the dashboard to be updated after saving options
     * @return void
     */
    public function woo_save_settings()
    {
        if ( @$_GET[ 'section' ] === 'nexostore-general' ) {
            /**
             * Why are we doing this ? Well, after the option are saved using WooCommerce API... we cannot refresh (hide/show)
             * the already generated CPT. So we need to refresh the settings page. But this process is slow.
             */
            wp_redirect( admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-general&notice=settings-saved' ) );
        }
    }    

    /**
     * Calculating total
     * to update each item price according to the modifiers
     * @return void
     */
    public function calculate_totals( WC_Cart $cart ) 
    {
        if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
            return;
        }

        if ( ! $cart->is_empty() ) {
            foreach( $cart->get_cart() as $key => $value ) {
                if( isset( $value[ 'nexostore-item-price' ] ) ) {
                    $price = $value[ 'nexostore-item-price' ];
                    $value[ 'data' ]->set_price( ( $price ) );
                }
            }    
        }    
    }

    /**
     * Add modifier to order 
     * @return void
     */
    public function add_modifier_data_to_order( $item, $cart_item_key, $values, $order ) 
    {
        if( isset( $values[ 'nexostore-modifiers' ] ) ) {
            foreach( $values[ 'nexostore-modifiers' ] as $group ) {
                foreach( $group[ 'modifiers' ] as $modifier ) {
                    $item->add_meta_data( $group[ 'group_name' ], $modifier[ 'name' ] . ' &mdash; ' . wc_price( $modifier[ 'price' ] ) );
                }
            }
            
            $item->add_meta_data( '$modifiers', json_encode( $values[ 'nexostore-modifiers' ]) );
        }
    }

    /**
     * Show Modifier Selector
     * @return void
     */
    public function show_modifier_selector() 
    {
        global $post;

        $product            =   wc_get_product( $post->ID );
        $modifiers_groups   =   Product::get_product_modifiers( $post->ID );
        
        /**
         * Define a text domain for the JS Vue object
         */
        $textDomain             =   [
            'loading'           =>  __( 'Loading...', 'nexostore' ),
            'chooseModifier'    =>  __( 'Add to the Cart', 'nexostore' ),
        ];

        $this->view
            ->path( 'woocommerce' )
            ->data( compact( 'product', 'modifiers_groups', 'textDomain' ) )
            ->single_modifier();
    }
}