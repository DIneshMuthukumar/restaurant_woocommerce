<?php
/**
 * View Class
 *
 * @package     WooCommerce/Admin
 * @version     2.6.0
 */
namespace NexoStore\Classes;

defined( 'ABSPATH' ) || exit;

class View 
{
    /**
     * @var array
     * hold defined variables
     */
    private $data   =   null;

    /**
     * @var string
     * path where to find the view
     */
    private $path   =   '';

    public function __call( $name, $arguments = [])
    {
        if( $this->data != null ) {
            extract( $this->data );
        }

        if ( ! empty( $arguments ) ) {
            extract( $arguments );
        }

        $this->path     = is_string( $this->path ) ? $this->path : '';

        // retore the path
        include( NEXOSTORE_VIEW_PATH . DIRECTORY_SEPARATOR . $this->path . DIRECTORY_SEPARATOR . $name . '.php' );
        $this->path     =   '';
    }

    /**
     * Define data which are exposed on the view
     * @param any
     * @return void
     */
    public function data( array $data ) 
    {
        $this->data     =   $data;
        return $this;
    }

    /**
     * Define the path which should be used
     * to retreive the view
     * @return void
     */
    public function path( $path )
    {
        $this->path     =   $path;
        return $this;
    }
}