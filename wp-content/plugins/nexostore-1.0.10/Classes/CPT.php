<?php
namespace NexoStore\Classes;

use WP_Query;
use WC_Admin_Settings;

class CPT
{
    /**
     * Register Modifiers CPT
     * only if Gastro feature is enabled
     * @return void
     */
    public function register_modifiers()
    {
        if( get_option( 'nexostore_setup_mode', 'nexopos' ) === 'gastro' ) {

            /**
             * Register Taxonomy Labels
             */
            $taxonomyLabels     =   [
                'name'              => _x( 'Modifiers Groups', 'restaurant modifiers groups', 'nexostore' ),
                'singular_name'     => _x( 'Modifier Group', 'restaurant modifiers group', 'nexostore' ),
                'search_items'      => __( 'Search Modifiers Group', 'nexostore' ),
                'all_items'         => __( 'All Modifiers Group', 'nexostore' ),
                'parent_item'       => __( 'Parent Modifiers Group', 'nexostore' ),
                'parent_item_colon' => __( 'Parent Modifiers Group:', 'nexostore' ),
                'edit_item'         => __( 'Edit Modifiers Group', 'nexostore' ),
                'update_item'       => __( 'Update Modifiers Group', 'nexostore' ),
                'add_new_item'      => __( 'Add New Modifiers Group', 'nexostore' ),
                'new_item_name'     => __( 'New Modifiers Group Name', 'nexostore' ),
                'menu_name'         => __( 'Modifiers Groups', 'nexostore' ),
            ];

            $taxonomyArgs           =   [
                'hierarchical'      => false,
                'labels'            => $taxonomyLabels,
                'show_ui'           => true,
                'show_admin_column' => true,
                'show_in_menu'      => true,
                'query_var'         => false,
                'rewrite'           => array( 'slug' => 'nexostore-modifiers-group' ),
            ];

            register_taxonomy( 'nexostore-modifiers-groups', [ 'nexostore-modifiers' ], $taxonomyArgs );

            /**
             * register Custom Post Type
             */            
            $labels     =   [
                'name'               => _x( 'Modifiers', 'modifiers', 'nexostore' ),
                'singular_name'      => _x( 'Modifier', 'modifier', 'nexostore' ),
                'menu_name'          => _x( 'Modifiers', 'admin menu', 'nexostore' ),
                'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'nexostore' ),
                'add_new'            => _x( 'Add new', 'book', 'nexostore' ),
                'add_new_item'       => __( 'Add New Modifier', 'nexostore' ),
                'new_item'           => __( 'New Modifier', 'nexostore' ),
                'edit_item'          => __( 'Edit Modifier', 'nexostore' ),
                'view_item'          => __( 'View Modifier', 'nexostore' ),
                'all_items'          => __( 'All Modifiers', 'nexostore' ),
                'search_items'       => __( 'Search Modifiers', 'nexostore' ),
                'parent_item_colon'  => __( 'Parent Modifiers:', 'nexostore' ),
                'not_found'          => __( 'No Modifiers found.', 'nexostore' ),
                'not_found_in_trash' => __( 'No Modifiers found in Trash.', 'nexostore' )
            ];

            $args   =   [
                'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-nexostore' ),
                'public'             => true,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                // 'taxonomies'         => [ 'nexostore-modifiers-groups' ],
                'rewrite'            => array( 'slug' => 'nexostore-modifiers' ),
                'capability_type'    => 'post',
                'has_archive'        => false,
                'hierarchical'       => false,
                'menu_position'      => 56,
                'supports'           => array( 'title', 'author', 'thumbnail' )
            ];

            register_post_type( 'nexostore-modifiers', $args );
        }
    }

    /**
     * Save Modifier CPT
     * @return void
     */
    public function save_modifier_group( $termid )
    {
        if( isset( $_POST[ 'modifier_group_forced' ] ) ) {
            update_term_meta( $termid, 'modifier_group_forced', sanitize_text_field( $_POST[ 'modifier_group_forced' ] ) );
        }

        if( isset( $_POST[ 'modifier_group_multiselect' ] ) ) {
            update_term_meta( $termid, 'modifier_group_multiselect', sanitize_text_field( $_POST[ 'modifier_group_multiselect' ] ) );
        }
    }

    /**
     * Save Modifier
     * @return void
     */
    public function save_modifier( $post_id )
    {
        if ( @$_POST[ 'post_type' ] === 'nexostore-modifiers' ) {
            
            if ( get_option( 'nexostore_multiple_modifiers_by_default', 'no' ) === 'yes' ) {
                
                /**
                 * Let's update all available field and disable the default
                 * selected modifier
                 */
                $modifiers  =   new WP_Query([
                    'meta_query'    =>  [
                        [
                            'key'   =>  'nexostore_modifier_selected',
                            'value' =>  'yes',
                            'compare'   =>  '='
                        ], [
                            'key'   =>  'nexostore_modifier_group_id',
                            'value' =>  sanitize_text_field( @$_POST[ 'nexostore_modifier_group_id' ] ),
                            'compare'   =>  '='
                        ]
                    ],
                    'post_type'     =>  'nexostore-modifiers'
                ]);
    
                /**
                 * Only one post can be selected by default.
                 */
                foreach( $modifiers->get_posts() as $modifier ) {
                    /**
                     * We would like to avoid updating the current post
                     */
                    if ( $modifier->ID !== intval( $post_id ) ) {
                        update_post_meta( $post_id, 'nexostore_modifier_selected', 'no' );
                    }
                }
            }
            
            foreach([ 
                'nexostore_modifier_price', 
                'nexostore_modifier_selected', 
                'nexostore_modifier_group_id', 
                'nexostore_modifier_description',
                'nexostore_modifier_id' 
            ] as $field ) {
                update_post_meta( $post_id, $field, sanitize_text_field( $_POST[ $field ] ) );
            }
        }
    }
}