<?php
/**
 * Field Class
 * @author Blair Jersyer
 * @package 1.0
 * @since 1.0
 */
namespace NexoStore\Classes;

use WP_Error;

class Fields
{
    private $view;

    public function __construct()
    {
        $this->view     =   new View;
    }

    /**
     * Add new fields on the modifiers post type
     * @return void
     */
    public function register_post_type_fields()
    {

    }

    /**
     * add a link fields
     * @return view
     */
    public function link( $field )
    {
        include( dirname( __FILE__ ) . '/../views/custom/link.php' );
    }

    /**
     * register taxonomy fields
     * @return void
     */
    public function register_create_taxonomy_fields()
    {
        $this->view->taxonomy_create_fields();
    }

    /**
     * register edit taxonomy fields
     * @return void
     */
    public function register_edit_taxonomy_fields( $term )
    {
        $this->view->data( compact( 'term' ) )
            ->taxonomy_edit_fields();
    }

    
    /**
     * Add Field to WooCommerce product
     * @return void
     */
    public function woo_add_field()
    {
        global $post;

        $rawOptions         =   get_terms([
            'taxonomy'      =>  'nexostore-modifiers-groups',
            'hide_empty'    =>  false
        ]);

        if ( ! $rawOptions instanceof WP_Error ) {
            /**
             * Check if the CPT has groups, if not let the user know
             * why the group aren't created.
             */
            if( empty( $rawOptions ) ) {
                $options    =   [
                    '0'    =>  __( 'No modifier group has been created', 'nexostore' )
                ];
            } else {
                $options    =   [];
                foreach( $rawOptions as $group ) {
                    $options[ $group->term_id ]   =   $group->name;
                }
            }
    
            $args = [
                'id'                    => 'nexostore_selected_modifier_group[]',
                'label'                 => __( 'Modifier Group', 'nexostore' ),
                'class'                 => 'cfwc-custom-field',
                'options'               =>  $options,
                'desc_tip'              => true,
                'value'                 =>  get_post_meta( $post->ID, 'nexostore_selected_modifier_group', true ),
                'custom_attributes'     =>  [
                    'multiple'          =>  'multiple'
                ],
                'description'           => __( 'Select the modifiers which should be assigned to the product.', 'nexostore' ),
            ];
    
            woocommerce_wp_select( $args );
        }
    }

    /**
     * Save Product
     * @param int post id
     * @return void
     */
    public function save_product( $post_id )
    {
        if( isset( $_POST[ 'post_type' ] ) && $_POST[ 'post_type' ] === 'product' ) {
            foreach([
                'nexostore_selected_modifier_group'
            ] as $field ) {
                /**
                 * Sanitize the array of fields
                 */
                if ( isset( $_POST[ $field ] ) ) {
                    $data   =   $_POST[ $field ];
                    foreach( $data as &$_field ) {
                        $_field  =   sanitize_text_field( $_field );
                    }
    
                    update_post_meta( $post_id, $field, $data );
                }
            }
        }
    }

    /**
     * Add Checkout Fields
     * @return void
     */
    public function add_checkout_fields( $checkout )
    {
        // echo "<pre>";
        // print_r(WC()->session->nexostore_datetime);
        // die();
        foreach([
            'nexostore_datetime' ,
            'nexostore_table_seats' ,
            'nexostore_table_id' ,
            'nexostore_order_type' ,
        ] as $field ) {
            echo '<input name="' . $field . '" type="hidden" value="' . WC()->session->$field . '"/>';
        }
    }
}