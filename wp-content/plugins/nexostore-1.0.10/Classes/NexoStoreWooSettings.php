<?php
/**
 * WooCommerce Shipping Settings
 *
 * @package     NexoStore\Classes
 * @version     1.0
 */
namespace NexoStore\Classes;

use WC_Settings_Page;
use WC_Cache_Helper;
use WC_Admin_Settings;

defined( 'ABSPATH' ) || exit;

if ( class_exists( 'NexoStoreWooSettings', false ) ) {
	return new NexoStoreWooSettings();
}

/**
 * WC_Settings_Shipping.
 */
class NexoStoreWooSettings extends WC_Settings_Page {
	/**
	 * Sections which should render a HTML
	 * page instead of a configuration
	 */
	private $domSections 	=	[ 'nexostore-sync' ];

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    			= 'nexostore';
		$this->client_key 		=	'3VWg7wtEwjqzVHKvTIOuOaIZzzSXhc1gakngjXfo';
		$this->client_secret 	=	'yC0qCwbuOdz5JeJ37tGl8mX2cdRH8t60ovo0efje';
		$this->store     		=   NEXOSTORE_STORE_URL;
		$this->label 			= __( 'Nexo Store', 'nexostore' );
		$this->sections 		=	array(
			'nexostore-general' => __( 'General', 'nexostore' ),
			'nexostore-sync' 	=> __( 'Synchronisation', 'nexostore' ),
			'nexostore-licence' => __( 'Authentication', 'nexostore' ),
		);

		parent::__construct();
	}

	/**
	 * Add this page to settings.
	 *
	 * @param array $pages Current pages.
	 * @return array|mixed
	 */
	public function add_settings_page( $pages ) {
		return parent::add_settings_page( $pages );
	}

	/**
	 * Get sections.
	 *
	 * @return array
	 */
	public function get_sections() {
		return apply_filters( 'woocommerce_get_sections_' . $this->id, $this->sections );
	}

	/**
	 * Get settings array.
	 *
	 * @param string $current_section Current section.
	 * @return array
	 */
	public function get_settings( $current_section = '' ) {
		$fields 		=	[];

		include( NEXOSTORE_ROOT_PATH . '/inc/Fields.php' );

		foreach( array_keys( $this->sections ) as $section ) {
			$settings = apply_filters(
				$section . '-settings', @$fields[ $current_section ]
			);
		}

		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
	}

	/**
	 * Output sections.
	 */
	public function output_sections() {
		global $current_section;

		if ( @$_GET[ 'section' ] === null ) {
			return wp_redirect( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-general' );
		}

		$sections = $this->get_sections();
		if ( empty( $sections ) || 1 === sizeof( $sections ) ) {
			return;
		}
		echo '<ul class="subsubsub">';
		$array_keys = array_keys( $sections );
		foreach ( $sections as $id => $label ) {
			echo '<li><a href="' . admin_url( 'admin.php?page=wc-settings&tab=' . $this->id . '&section=' . sanitize_title( $id ) ) . '" class="' . ( $current_section == $id ? 'current' : '' ) . '">' . $label . '</a> ' . ( end( $array_keys ) == $id ? '' : '|' ) . ' </li>';
		}
		echo '</ul><br class="clear" />';
	}

	/**
	 * Output the settings.
	 */
	public function output() {
		global $current_section, $hide_save_button;

		$this->manage_platform_auth();

		foreach( array_keys( $this->sections ) as $section ) {
			if ( $section === $current_section ) {
				if ( ! in_array( $section, $this->domSections ) ) {
					$settings = $this->get_settings( $current_section );
					\WC_Admin_Settings::output_fields( $settings );
				} else {
					$hide_save_button 	=	true;
					include_once( dirname( __FILE__ ) . '/../views/tabs/' . $section . '.php' );
				}
			} 
		}
	}

	/**
	 * Save Access Code
	 */
	public function manage_platform_auth()
	{
		global $current_section;

		if( $current_section == 'nexostore-licence' ) {
			$callback_url 	=	admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence' );
			$redirect_url 	=	$this->store . 'oauth?scopes=nexopos.envato-licences,nexopos.sync&client_secret=' . $this->client_secret . '&client_key=' . $this->client_key . '&callback_url=' . urlencode( $callback_url );
			$logout_url 	=	admin_url( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&platform-logout=true' );

			switch( @$_POST[ 'nexopos_goto' ] ) {
				case 'nexopos_platform_auth' : return wp_redirect( $redirect_url );
				case 'nexopos_platform_logout'	: return wp_redirect( $logout_url );
			}
		}	

		if ( isset( $_GET[ 'access_token' ] ) ) {
			if ( get_option( 'nexostore_access_token') !== false ) {
				update_option( 'nexostore_access_token', sanitize_text_field( $_GET[ 'access_token' ] ) );
			} else {
				add_option( 'nexostore_access_token', sanitize_text_field( $_GET[ 'access_token' ] ) );
			}

			return wp_redirect( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&platform-connected=true' );
		}

		if ( isset( $_GET[ 'platform-logout' ] ) ) {

			/**
			 * The access token should be deleted
			 * if the user revoke the connexion
			 */
			delete_option( 'nexostore_access_token' );

			/**
			 * if that option exists, 
			 * we should basically restrict installation selection.
			 * the user'll have to revoke the connexion before
			 */
			delete_option( 'nexostore_installation_details' );
			
			return wp_redirect( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&platform-logged-out=true' );
		}
	}

	/**
	 * Save settings.
	 */
	public function save() {
		global $current_section;
		
		switch ( $current_section ) {
			case 'options':
				WC_Admin_Settings::save_fields( $this->get_settings() );
			break;
			case 'nexostore-general':
				WC_Admin_Settings::save_fields( $this->get_settings( $current_section ) );
			break;
			default:
				
			break;
		}

		if ( $current_section ) {
			do_action( 'woocommerce_update_options_' . $this->id . '_' . $current_section );
		}
	}
}