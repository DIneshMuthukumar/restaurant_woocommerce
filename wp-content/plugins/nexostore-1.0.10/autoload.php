<?php
/**
 * Autoloading for class within 
 * the system
 */
defined( 'ABSPATH' ) || exit;

include_once( 'vendor/autoload.php' );

spl_autoload_register(function ($name) {
    $name   =   str_replace( 'NexoStore\\', '', $name );
    $file   =   str_replace( '\\', DIRECTORY_SEPARATOR, $name).'.php';

    if ( file_exists( NEXOSTORE_ROOT_PATH . $file ) ) {
        require_once( NEXOSTORE_ROOT_PATH . $file );
        return true;
    }
    return false;
});
