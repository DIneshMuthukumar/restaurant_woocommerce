<?php defined( 'ABSPATH' ) || exit;?>
<?php
/**
 * if the application is not activated, we should redirect 
 * the user to the activation section
 * @todo
 */
?>
<form action="" method="post">
    <div class="pt-3">
        <div class="row">
            <div class="col-3">
                <div class="field-input">
                    <label for="text"><?php echo __( 'Purchase Code', 'nexostore' );?></label>
                    <input name="text" type="text" value="" class="regular-text" />
                    <div class="description"><?php esc_attr_e( 'This is a description for a form element.', 'WpAdminStyle' ); ?></div>
                </div>
            </div>
            <div class="col-3">
                <div class="field-input">
                    <label for="text">This is the label</label>
                    <input name="text" type="text" value="" class="regular-text" />
                    <div class="description"><?php esc_attr_e( 'This is a description for a form element.', 'WpAdminStyle' ); ?></div>
                </div>
            </div>
            <div class="col-3">
                <div class="field-input">
                    <label for="text">This is the label</label>
                    <input name="text" type="text" value="" class="regular-text" />
                    <div class="description"><?php esc_attr_e( 'This is a description for a form element.', 'WpAdminStyle' ); ?></div>
                </div>
            </div>
            <div class="col-3">
                <div class="field-input">
                    <label for="text">This is the label</label>
                    <input name="text" type="text" value="" class="regular-text" />
                    <div class="description"><?php esc_attr_e( 'This is a description for a form element.', 'WpAdminStyle' ); ?></div>
                </div>
            </div>
            <div class="col-3">
                <div class="field-input">
                    <label for="text">This is the label</label>
                    <input name="text" type="text" value="" class="regular-text" />
                    <div class="description"><?php esc_attr_e( 'This is a description for a form element.', 'WpAdminStyle' ); ?></div>
                </div>
            </div>
        </div>
        <?php echo submit_button();?>
    </div>
</form>