<?php defined( 'ABSPATH' ) || exit;?>
<?php
if ( get_option( 'nexostore_installation_details', null ) === null ) {
    return wp_redirect( 'admin.php?page=wc-settings&tab=nexostore&section=nexostore-licence&not-authenticated=true' );
}
?>
<div id="nexostore-sync-template">
    <table class="form-table">
        <tbody>
            <tr valign="top" class="">
                <th scope="row" class="titledesc" style="width: 300px;">
                    <?php echo __( 'Sync Options', 'nexostore' );?><br>
                    <small><?php echo __( 'The sync proceess might delete all item to the destination', 'nexostore' );?></small>
                </th>
                <td class="forminp forminp-checkbox">
                    <fieldset>
                        <legend class="screen-reader-text">
                            <span><?php echo __( 'Sync Options', 'nexostore' );?></span>
                        </legend>
                        <input :readonly="has_started" v-model="nexostore_sync_option" name="nexostore_sync_option" id="sync_to_nexopos" type="radio"
                            class="" checked="checked" value="sync_to_nexopos">
                        <label for="sync_to_nexopos"><?php echo __( 'Sync from WooCommerce to NexoPOS.', 'nexostore' );?></label>
                    </fieldset>
                    <fieldset class="">
                        <input :readonly="has_started" v-model="nexostore_sync_option" name="nexostore_sync_option" id="sync_to_woocommerce" type="radio" class=""
                            value="sync_to_woocommerce">
                        <label for="sync_to_woocommerce"><?php echo __( 'Sync from NexoPOS to WooCommerce.', 'nexostore' );?></label>
                    </fieldset>
                    <!-- <fieldset class="">
                        <input v-model="nexostore_sync_option" name="nexostore_sync_option" id="merge" type="radio" class=""
                            value="merge"> 
                        <label for="merge"><?php echo __( 'Merge products from NexoPOS to WooCommerce and vice versa', 'nexostore' );?></label>
                    </fieldset> -->
                </td>
            </tr>
            <tr valign="top" class="">
                <th scope="row" class="titledesc"><?php echo __( 'Assign Author', 'nexostore' );?></th>
                <td class="forminp forminp-checkbox">
                    <fieldset>
                        <legend class="screen-reader-text">
                            <span><?php echo __( 'Assign Author', 'nexostore' );?></span>
                        </legend>
                        <label for="woocommerce_enable_checkout_login_reminder">
                            <?php wp_dropdown_users([
                                'class' =>  'users-list'
                            ]);?>
                            <br>
                            <small><?php echo __( 'This users will be assigned as author of the new items imported.', 'nexostore' );?></small>
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top" class="">
                <th scope="row" class="titledesc">
                    <button 
                        :disabled="has_started"
                        @click="launchSync()"
                        name="save" 
                        class="button-primary" 
                        type="button" value="Enregistrer les changements"><?php echo __( 'Launch the Synchronisation', 'nexostore' );?></button>
                </th>
            </tr>
            <tr v-if="has_started">
                <th colspan="2">
                    <small><?php echo sprintf( __( 'Overall Progress : %s %% &mdash; %s', 'nexostore' ), '{{ progress_percentage.toFixed(2) }}', '{{ current_process_title }}' );?></small>
                    <div 
                        id="progressbar" 
                        class="ui-progressbar ui-widget ui-widget-content ui-corner-all" 
                        role="progressbar" 
                        aria-valuemin="0" 
                        aria-valuemax="100" aria-valuenow="20">
                        <div class="ui-progressbar-value ui-widget-header ui-corner-left" v-bind:style="{ width : progress_percentage + '%' }"></div>
                    </div>
                    <small><?php echo __( 'Don\'t close the browser during the process', 'nexostore' );?></small>
                </th>
            </tr>
        </tbody>
    </table>
</div>