<?php defined( 'ABSPATH' ) || exit;?>
<div style="margin: 10px 0px">
    <label style="margin-bottom: 5px;display: block" for="<?php echo $field[ 'name' ];?>"><strong><?php echo $field[ 'label' ];?></strong></label>
    <select 
        style="width: 99%" 
        name="<?php echo esc_attr( $field[ 'name' ] );?>" id="">
        <?php foreach( ( array ) @$field[ 'options' ] as $key => $value ):?>
            <option 
                <?php echo strval( $key ) == @$field[ 'value' ] ? 'selected="selected"' : '';?> 
                value="<?php echo esc_attr( $key );?>">
                <?php echo esc_html( $value );?>
            </option>
        <?php endforeach;?>
    </select>
    <p style="margin:0px"><?php echo @$field[ 'description' ];?></p>
</div>