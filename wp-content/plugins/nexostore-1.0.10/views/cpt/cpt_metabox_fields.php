<?php defined( 'ABSPATH' ) || exit;?>
<?php if ( @$fields ):?>
    <?php foreach( $fields as $field ):?>
        <?php if ( in_array( $field[ 'type' ], [ 'text', 'email', 'password', 'hidden' ] ) ):?>
            <?php $view->data( compact( 'field' ) )->path( 'cpt' )->cpt_text_field();?>
        <?php endif;?>
        <?php if ( in_array( $field[ 'type' ], [ 'select' ] ) ):?>
            <?php $view->data( compact( 'field' ) )->path( 'cpt' )->cpt_select_field();?>
        <?php endif;?>
        <?php if ( in_array( $field[ 'type' ], [ 'image' ] ) ):?>
            <?php $view->data( compact( 'field' ) )->path( 'cpt' )->cpt_image_field();?>
        <?php endif;?>
        <?php if ( in_array( $field[ 'type' ], [ 'textarea' ] ) ):?>
            <?php $view->data( compact( 'field' ) )->cpt_textarea_field();?>
        <?php endif;?>
        <?php if ( in_array( $field[ 'type' ], [ 'editor' ] ) ):?>
            <?php $view->data( compact( 'field' ) )->path( 'cpt' )->cpt_editor();?>
        <?php endif;?>
    <?php endforeach;?>
<?php endif;?>