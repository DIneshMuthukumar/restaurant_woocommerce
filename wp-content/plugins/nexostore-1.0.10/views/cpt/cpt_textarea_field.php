<?php defined( 'ABSPATH' ) || exit;?>
<div style="margin: 10px 0px">
    <label style="margin-bottom: 5px;display: block" for="<?php echo $field[ 'name' ];?>"><strong><?php echo $field[ 'label' ];?></strong></label>
    <textarea 
        type="<?php echo @$field[ 'type' ];?>" 
        placeholder="<?php echo esc_attr( @$field[ 'placeholder' ] );?>" 
        name="<?php echo esc_attr( @$field[ 'name' ] );?>" 
        value="<?php esc_attr_e( @$field[ 'value' ] );?>" 
        class="large-text">
    </textarea>
    <p style="margin:0px"><?php echo esc_html( @$field[ 'description' ] );?></p>
</div>