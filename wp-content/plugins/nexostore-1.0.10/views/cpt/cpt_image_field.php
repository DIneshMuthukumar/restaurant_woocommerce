<?php 
defined( 'ABSPATH' ) || exit;
global $post;

$field_name         =   'select-media-' . $field[ 'name' ];
$upload_link        =   esc_url( get_upload_iframe_src( 'image', $post->ID ) );
$get_image_id       =   get_post_meta( $post->ID, $field[ 'name' ], true );
$get_image_src      =   wp_get_attachment_image_src( $get_image_id, 'full' );
$image_set          =   is_array( $get_image_src );
$upload_class       =   $field[ 'name' ] . '-upload-image';
$remove_class       =   $field[ 'name' ] . '-delete-image';
$img_container      =   $field[ 'name' ] . '-img-container';
$img_hidden         =   $field[ 'name' ] . '-img-hidden';
?>
<div class="<?php esc_attr_e( $img_container );?>">
    <?php if ( $image_set ) : ?>
        <img src="<?php echo $image_set[0] ?>" alt="" style="max-width:50%;" />
    <?php endif; ?>
</div>

<p class="hide-if-no-js">
    <a class="button-secondary <?php esc_attr_e( $upload_class );?> <?php if ( $image_set  ) { echo 'hidden'; } ?>" href="<?php echo $upload_link ?>">
        <?php esc_attr_e( @$field[ 'label' ] ?: __( 'Select an image', 'nexostore' ) ); ?>
    </a>
    <a class="button-secondary <?php esc_attr_e( $remove_class );?> <?php if ( ! $image_set  ) { echo 'hidden'; } ?>" href="#">
        <?php echo __( 'Remove this image', 'nexostore' );?>
    </a>
</p>

<input class="<?php esc_attr_e( $img_hidden );?>" name="<?php echo $field[ 'name' ];?>" type="hidden" value="<?php echo esc_attr( $get_image_id ); ?>" />

<script>
    jQuery(function ($) {

        // Set all variables to be used in scope
        var frame,
            metaBox         = $('.postbox'),
            addImgLink      = metaBox.find('.<?php echo $upload_class;?>'),
            delImgLink      = metaBox.find('.<?php echo $remove_class;?>'),
            imgContainer    = metaBox.find('.<?php echo $img_container;?>'),
            imgIdInput      = metaBox.find('.<?php echo $img_hidden;?>');

        // ADD IMAGE LINK
        addImgLink.on('click', function (event) {

            event.preventDefault();

            // If the media frame already exists, reopen it.
            if (frame) {
                frame.open();
                return;
            }

            // Create a new media frame
            frame = wp.media({
                title: 'Select or Upload Media Of Your Chosen Persuasion',
                button: {
                    text: 'Use this media'
                },
                multiple: false // Set to true to allow multiple files to be selected
            });


            // When an image is selected in the media frame...
            frame.on('select', function () {

                // Get media attachment details from the frame state
                var attachment = frame.state().get('selection').first().toJSON();

                // Send the attachment URL to our custom image input field.
                imgContainer.append('<img src="' + attachment.url +
                    '" alt="" style="max-width:50%;"/>');

                // Send the attachment id to our hidden input
                imgIdInput.val(attachment.id);

                // Hide the add image link
                addImgLink.addClass('hidden');

                // Unhide the remove image link
                delImgLink.removeClass('hidden');
            });

            // Finally, open the modal on click
            frame.open();
        });


        // DELETE IMAGE LINK
        delImgLink.on('click', function (event) {

            event.preventDefault();

            // Clear out the preview image
            imgContainer.html('');

            // Un-hide the add image link
            addImgLink.removeClass('hidden');

            // Hide the delete image link
            delImgLink.addClass('hidden');

            // Delete the image id from the hidden input
            imgIdInput.val('');

        });

    });
</script>