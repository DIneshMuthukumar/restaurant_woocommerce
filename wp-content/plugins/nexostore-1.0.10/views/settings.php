<?php defined( 'ABSPATH' ) || exit;?>
<h1><?php echo __( 'Nexo Store Settings', 'nexostore' );?></h1>
<?php include( dirname( __FILE__ ) . '/settings-header.php' );?>
<?php include( dirname( __FILE__ ) . '/settings-tabs.php' );?>