<?php defined( 'ABSPATH' ) || exit;?>
<?php foreach( $tabs as $namespace => $tab ):?>
    <?php if ( @$_GET[ 'tab' ] === $namespace ):?>
    <?php include( dirname( __FILE__ ) . '/tabs/' . $namespace . '.php' );?>
    <?php endif;?>
<?php endforeach;?>