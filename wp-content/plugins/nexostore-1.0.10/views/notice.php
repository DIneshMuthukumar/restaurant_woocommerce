<?php defined( 'ABSPATH' ) || exit;?>
<div class="notice <?php echo @$notice_type ?: 'notice-success';?> is-dismissible">
    <p><?php echo $message; ?></p>
</div>