<?php defined( 'ABSPATH' ) || exit;?>
<tr valign="top">
    <th scope="row" class="titledesc">
        <label for="<?php echo esc_attr( $value['id'] ); ?>"><?php echo esc_html( $value['title'] ); ?></label>
    </th>
    <td class="forminp forminp-<?php echo esc_attr( sanitize_title( $value['type'] ) ); ?>">
        <button 
            name="<?php echo esc_attr( $value['id'] ); ?>" 
            class="button-primary" 
            type="submit" 
            id="<?php echo esc_attr( $value['id'] ); ?>"
            type="<?php echo esc_attr( $value['type'] ); ?>"
            style="<?php echo esc_attr( $value['css'] ); ?>"
            value="<?php echo esc_attr( $value[ 'value' ] ); ?>"
            class="<?php echo esc_attr( $value['class'] ); ?>"
            placeholder="<?php echo esc_attr( $value['placeholder'] ); ?>"
            ><?php echo esc_attr( $value[ 'text' ] );?></button><br>
            <p><small style="padding-top: 10px"><?php echo esc_html( $value[ 'description' ] );?></small></p>
    </td>
</tr>