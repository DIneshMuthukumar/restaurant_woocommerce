<?php defined( 'ABSPATH' ) || exit;?>
<tr valign="top">
    <th scope="row" class="titledesc">
        <label for="nexostore_remote_websites">Select the installation</label>
    </th>
    <td class="forminp forminp-select">
        <select name="nexostore_remote_websites" id="nexostore_remote_websites" style="" class="">
            <?php foreach( $value[ 'options' ] as $value => $text ):?>
            <option value="<?php echo sanitize_text_field( $value );?>">
            <?php echo esc_html( $text );?>
            </option>
            <?php endforeach;?>
        </select>
        <!-- <a href=""" class="button-primary" type="button" style="" placeholder=""><?php echo __( 'Connect NexoPOS', 'nexostore' );?></a>  -->
        <button name="ping_website" class="button-primary" type="button" id="ping-gastro" style="" value="nexopos_platform_logout" placeholder=""><?php echo __( 'Ping NexoPOS', 'nexostore' );?></button> 
        <br>
        <span class="description"><?php echo __( 'With which installation would you like to connect to. That installation might have Gastro installed on it and must be hosted on a remote server', 'nexostore' );?></span>
    </td>
</tr>