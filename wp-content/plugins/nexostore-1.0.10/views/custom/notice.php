<?php defined( 'ABSPATH' ) || exit;?>
<div class="notice notice-success is-dismissible">
    <p><?php echo esc_html( $value );?></p>
    <button type="button" class="notice-dismiss">
        <span class="screen-reader-text"><?php echo __( 'Dismiss', 'nexostore' );?></span>
    </button>
</div>