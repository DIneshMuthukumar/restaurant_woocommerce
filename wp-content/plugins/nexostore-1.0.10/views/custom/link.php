<table class="form-table">
    <tbody>
        <tr valign="top">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo esc_html( $field['title'] ); ?></label>
            </th>
            <td class="forminp forminp-<?php echo esc_attr( sanitize_title( $field['type'] ) ); ?>">
                <a href="<?php echo esc_url( $field[ 'href' ]);?>"
                    class="button-primary"><?php echo esc_html( $field[ 'text' ] );?></a><br>
                <p><small style="padding-top: 10px"><?php echo esc_html( $field[ 'description' ] );?></small></p>
            </td>
        </tr>
    </tbody>
</table>