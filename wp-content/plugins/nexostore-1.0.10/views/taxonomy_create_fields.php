<?php
defined( 'ABSPATH' ) || exit;

$forced             =   get_term_meta( @$term->term_id, 'modifier_group_forced', true );
$multtiselect       =   get_term_meta( @$term->term_id, 'modifier_group_multiselect', true );
?>

<div class="form-field">
    <label for="modifier_group_forced"><?php _e( 'Forced', 'nexostore' ); ?></label>
    <select name="modifier_group_forced" style="width: 95%">
        <option <?php echo $forced === 'yes' ? 'selected="selected"' : '';?> value="yes"><?php echo __( 'Yes', 'nexostore' );?></option>
        <option <?php echo $forced === 'no' ? 'selected="selected"' : '';?> value="no"><?php echo __( 'No', 'nexostore' );?></option>
    </select>
    <p><?php echo __( 'Let you define wether the group is forced or can be skipped while selecting the product.', 'nexostore' );?></p>
</div>

<div class="form-field">
    <label for="modifier_group_multiselect"><?php _e( 'Multi Select', 'nexostore' ); ?></label>
    <select name="modifier_group_multiselect" style="width: 95%">
        <option <?php echo $multtiselect === 'yes' ? 'selected="selected"' : '';?> value="yes"><?php echo __( 'Yes', 'nexostore' );?></option>
        <option <?php echo $multtiselect === 'no' ? 'selected="selected"' : '';?> value="no"><?php echo __( 'No', 'nexostore' );?></option>
    </select>
    <p><?php echo __( 'Whether the group allow multiple modifiers to be selected.', 'nexostore' );?></p>
</div>