<?php defined( 'ABSPATH' ) || exit;?>
<div class="notice notice-success is-dismissible">
    <p><?php _e( 'The connexion with Nexo Platform is successful.', 'nexostore' ); ?></p>
</div>