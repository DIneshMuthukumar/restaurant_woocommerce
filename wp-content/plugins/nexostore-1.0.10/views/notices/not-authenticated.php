<?php defined( 'ABSPATH' ) || exit;?>
<div class="notice notice-error is-dismissible">
    <p><?php echo sprintf( __( 'It seems like you\'ve not yet linked your WooCommerce store to NexoPOS. read <a href="%s">this for more instructions</a>.', 'nexostore' ), 'https://nexopos.com/how-to-authenticate-nexo-store-with-nexo-platform/' ); ?></p>
</div>