<?php defined( 'ABSPATH' ) || exit;?>
<?php if ( $modifiers_groups !== false ):?>
<div id="nexostore-single-product-modifier" class="bootstrapiso" style="display:none">
    <div class="group-single">
        <h4><?php echo __( 'Modifier', 'nexostore' );?>: {{ groups[ index ].group_name }}</h4>
        <p>{{ groups[ index ].group_description }}</p>
        <div class="row">
            <div class="col-md-4" v-for="modifier in groups[ index ].modifiers" style="margin-bottom: 25px">
                <div class="modifier modifier-gray" :class="{ 'active' : modifier.selected === 'yes' }" @click="toggleModifier( modifier )">
                    <img :src="modifier.thumbnail" alt="" srcset="">
                    <p style="text-align:center;margin:0">{{ modifier.name }}</p>
                    <p  style="text-align:center;margin:5px 0 10px">
                        {{ currency_symbol }} {{ modifier.price }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
<br>
<style>
    #nexostore-single-product-modifier {
        padding: 5px;
    }
</style>
<script>
    /**
     * Let's extend the declared JS object 
     * with some new data
     */
    NexoStoreSingleProductData    =   Object.assign(NexoStoreSingleProductData, {
        groups: <?php echo json_encode( $modifiers_groups );?>
    });
</script>
<?php endif;?>