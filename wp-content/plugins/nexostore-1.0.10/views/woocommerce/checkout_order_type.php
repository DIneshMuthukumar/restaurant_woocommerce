<?php defined( 'ABSPATH' ) || exit;?>
<div id="nexostore-order-type" class="bootstrapiso" style="display:none">
    <div v-if="hasServerError" class="alert alert-danger" role="alert">
        <strong><?php echo __( 'Oops An error occured !', 'nexostore' );?></strong> 
        <?php echo __( 'We\'re tried to reach the server and this latest has returned the following error :', 'nexostore' );?> {{ serverErrorMessage }}
    </div>
    <div v-if="! showOrderTypes && ! isDineInOrder">
        <h4><?php echo sprintf( __( 'Your order :', 'nexostore' ) );?></h4>
        <p>
            <?php echo sprintf( __( '<strong>Type</strong> : %s', 'nexostore' ), '{{ selectedOrderType.text }}' );?> &mdash; 
            <a @click="cancelOrderType()" href="javascript:void(0)"><?php echo __( 'Cancel', 'nexostore' );?></a>
        </p>
    </div>
    <div v-if="! showOrderTypes && isDineInOrder">
        <h4><?php echo sprintf( __( 'Your order :', 'nexostore' ) );?></h4>
        <div class="row">
            <div class="col-md-6">
                <?php echo sprintf( __( '<strong>Type</strong> : %s', 'nexostore' ), '{{ selectedOrderType.text }}' );?>
                &mdash; <a @click="cancelOrderType()" href="javascript:void(0)"><?php echo __( 'Cancel', 'nexostore' );?></a>
            </div>
            <div class="col-md-6" v-if="selectedTable.TABLE_NAME">
                <?php echo sprintf( __( '<strong>Table</strong> : %s', 'nexostore' ), '{{ selectedTable.TABLE_NAME }}' );?><br>
            </div>
            <div class="col-md-6" v-if="selectedTable.TABLE_NAME">
                <?php echo sprintf( __( '<strong>Guest</strong> : %s', 'nexostore' ), '{{ selectedSeats }}' );?>
            </div>
            <div class="col-md-6" v-if="selectedDateTime">
                <?php echo sprintf( __( '<strong>Booked For</strong> : %s', 'nexostore' ), '{{ selectedDateTime.format() }}' );?>
            </div>
        </div>
    </div>
    <div v-if="showOrderTypes">
        <h4><?php echo __( 'Please select your order type', 'nexostore' );?></h4>
        <p><?php echo __( 'Right after, the cart will be visible.', 'nexostore' );?></p>
        <div class="row">
            <div class="col-md-4" style="margin-bottom: 30px;" v-for="type in orderTypes">
                <div class="card">
                    <img class="card-img-top" :src="type.image" :alt="type.name">
                    <div class="card-body">
                        <h5 class="card-title">{{ type.text }}</h5>
                        <p class="card-text">{{ type.description }}</p>
                        <a href="javascript:void(0)" @click="selectOrderType( type )" class="btn btn-primary"><?php echo __( 'Choose', 'nexostore' );?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="show-table" v-if="showTables">
        <div v-if="hasLoadedTables" class="has-loaded-tables">
            <div v-if="hasAvailableTables" class="has-available-tables">
                <h5><?php echo __( 'Select the table', 'nexostore' );?></h5>
                <p><?php echo __( 'Here are the following available. Note that each table doesn\'t have the same number of seat.', 'nexostore' );?></p>
                <div class="row">
                    <div class="col-md-4" style="margin-bottom: 30px;" v-for="table in tables">
                        <div class="card">
                            <img class="card-img-top" :src="table.image" :alt="table.name">
                            <div class="card-body">
                                <h5 class="card-title">{{ table.TABLE_NAME }}</h5>
                                <p class="card-text"><?php echo sprintf( __( 'Available Seats : %s', 'nexostore' ), '{{ table.MAX_SEATS }}' );?></p>
                                <button @click="showSeats( $event )" type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo __( 'Choose Seats', 'nexostore' );?>
                                </button>
                                <div class="dropdown-menu">
                                    <a @click="selectTableAndSeat( table, index + 1 )" v-for="( seat, index ) in getSeatArray( table )" class="dropdown-item" href="javascript:void(0)">{{ index + 1 }}</a>
                                </div>
                                <!-- <a href="javascript:void(0)" @click="selectTable( table )" class="btn btn-primary"><?php echo __( 'Choose', 'nexostore' );?></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div v-if="! hasAvailableTables" class="no-available-tables">
                <h5><?php echo __( 'Oops ! It seems like the restaurant doesn\'t have any table available', 'nexostore' );?></h5>
                <p><?php echo __( 'You can cancel the Dine in and choose something else.', 'nexostore' );?></p>
            </div>
        </div>
        <div v-if="! hasLoadedTables" class="loading-wrapper">
            <div class="load-table lds-roller" style="margin-bottom:20px;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
    </div>
    <div class="show-date-time" v-if="showDateTime">
        <div class="form-group">
            <div class="row">
                <div class="col-md-12">
                    <h5 v-if="[ 'dinein', 'takeaway' ].indexOf( selectedOrderType.namespace ) > -1"><?php echo __( 'When should we expect you ?', 'nexostore' );?></h5>
                    <h5 v-if="selectedOrderType.namespace === 'delivery'"><?php echo __( 'When do you expect to be delivered ?', 'nexostore' );?></h5>
                    <div class="card">
                        <div class="card-body">
                            <div id="nexostore-datetime"></div>
                            <button @click="selectDateTime()" class="btn btn-primary"><?php echo __( 'Confirm', 'nexostore' );?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="lds-roller main-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
<style>
.bootstrapiso .card {
    box-shadow: 4px 4px 5px -3px #e0e0e0;
    background-color: #fbfbfb;
}
.bootstrap-datetimepicker-widget table td.day {
    height: 26px;
    line-height: 30px;
    width: 20px;
}
.loading-wrapper {
    height: 300px;
    display: flex;
    flex-direction: column;
    align-content: center;
    align-items: center;
    justify-content: center;
}
.lds-roller {
    width: 64px;
    height: 64px
}
</style>