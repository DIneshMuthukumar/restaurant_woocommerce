<?php defined( 'ABSPATH' ) || exit;?>
<h2 class="nav-tab-wrapper">
	<?php foreach( $tabs as $namespace => $tab ):?>
    <a 
        href="<?php echo admin_url( 'options-general.php?page=nexostore-settings&tab=' . $namespace );?>" 
        class="nav-tab <?php echo ( @$_GET[ 'tab' ] === $namespace ) ? 'nav-tab-active' : '';?>">
            <?php echo $tab[ 'label' ];?>
    </a>
    <?php endforeach;?>
</h2>